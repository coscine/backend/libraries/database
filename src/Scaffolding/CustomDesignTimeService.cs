﻿using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.DependencyInjection;

namespace Coscine.Database
{
    public class CustomDesignTimeService : IDesignTimeServices
    {
        public void ConfigureDesignTimeServices(IServiceCollection serviceCollection)
                    => serviceCollection.AddSingleton<IPluralizer, CustomPluralizer>();
    }

    public class CustomPluralizer : IPluralizer
    {
        Inflector.Inflector _inflector = new Inflector.Inflector(System.Globalization.CultureInfo.GetCultureInfo("en-us"));

        public string Pluralize(string identifier)
        {
            return _inflector.Pluralize(identifier) ?? identifier;
        }

        public string Singularize(string identifier)
        {
            return _inflector.Singularize(identifier) ?? identifier;
        }
    }
}
