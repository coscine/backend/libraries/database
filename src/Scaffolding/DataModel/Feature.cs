﻿using System;
using System.Collections.Generic;

namespace Coscine.Database.DataModel
{
    public partial class Feature
    {
        public Feature()
        {
            ActivatedFeatures = new HashSet<ActivatedFeature>();
        }

        public Guid Id { get; set; }
        public string SharepointId { get; set; }
        public string DisplaynameEn { get; set; }
        public string DisplaynameDe { get; set; }

        public virtual ICollection<ActivatedFeature> ActivatedFeatures { get; set; }
    }
}
