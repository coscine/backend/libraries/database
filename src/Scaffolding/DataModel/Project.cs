﻿using System;
using System.Collections.Generic;

namespace Coscine.Database.DataModel
{
    public partial class Project
    {
        public Project()
        {
            ActivatedFeatures = new HashSet<ActivatedFeature>();
            Invitations = new HashSet<Invitation>();
            ProjectDisciplines = new HashSet<ProjectDiscipline>();
            ProjectInstitutes = new HashSet<ProjectInstitute>();
            ProjectPublicationRequests = new HashSet<ProjectPublicationRequest>();
            ProjectQuotas = new HashSet<ProjectQuota>();
            ProjectResources = new HashSet<ProjectResource>();
            ProjectRoles = new HashSet<ProjectRole>();
            SubProjectProjects = new HashSet<SubProject>();
            SubProjectSubProjectNavigations = new HashSet<SubProject>();
        }

        public Guid Id { get; set; }
        public string ProjectName { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Keywords { get; set; }
        public string DisplayName { get; set; }
        public string PrincipleInvestigators { get; set; }
        public string GrantId { get; set; }
        public Guid? VisibilityId { get; set; }
        public bool Deleted { get; set; }
        public string Slug { get; set; }
        public Guid? Creator { get; set; }
        public DateTime? DateCreated { get; set; }

        public virtual Visibility Visibility { get; set; }
        public virtual ICollection<ActivatedFeature> ActivatedFeatures { get; set; }
        public virtual ICollection<Invitation> Invitations { get; set; }
        public virtual ICollection<ProjectDiscipline> ProjectDisciplines { get; set; }
        public virtual ICollection<ProjectInstitute> ProjectInstitutes { get; set; }
        public virtual ICollection<ProjectPublicationRequest> ProjectPublicationRequests { get; set; }
        public virtual ICollection<ProjectQuota> ProjectQuotas { get; set; }
        public virtual ICollection<ProjectResource> ProjectResources { get; set; }
        public virtual ICollection<ProjectRole> ProjectRoles { get; set; }
        public virtual ICollection<SubProject> SubProjectProjects { get; set; }
        public virtual ICollection<SubProject> SubProjectSubProjectNavigations { get; set; }
    }
}
