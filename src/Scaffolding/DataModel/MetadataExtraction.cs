﻿using System;
using System.Collections.Generic;

namespace Coscine.Database.DataModel
{
    public partial class MetadataExtraction
    {
        public Guid Id { get; set; }
        public Guid ResourceId { get; set; }
        public bool Activated { get; set; }

        public virtual Resource Resource { get; set; }
    }
}
