﻿using System;
using System.Collections.Generic;

namespace Coscine.Database.DataModel
{
    public partial class GroupMembership
    {
        public Guid RelationId { get; set; }
        public Guid GroupId { get; set; }
        public Guid UserId { get; set; }

        public virtual Group Group { get; set; }
        public virtual User User { get; set; }
    }
}
