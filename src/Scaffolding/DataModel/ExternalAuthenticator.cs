﻿using System;
using System.Collections.Generic;

namespace Coscine.Database.DataModel
{
    public partial class ExternalAuthenticator
    {
        public ExternalAuthenticator()
        {
            ExternalIds = new HashSet<ExternalId>();
        }

        public Guid Id { get; set; }
        public string DisplayName { get; set; }

        public virtual ICollection<ExternalId> ExternalIds { get; set; }
    }
}
