﻿using System;
using System.Collections.Generic;

namespace Coscine.Database.DataModel
{
    public partial class Language
    {
        public Language()
        {
            Users = new HashSet<User>();
        }

        public Guid Id { get; set; }
        public string DisplayName { get; set; }
        public string Abbreviation { get; set; }

        public virtual ICollection<User> Users { get; set; }
    }
}
