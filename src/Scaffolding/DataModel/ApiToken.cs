﻿using System;
using System.Collections.Generic;

namespace Coscine.Database.DataModel
{
    public partial class ApiToken
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid UserId { get; set; }
        public DateTime IssuedAt { get; set; }
        public DateTime Expiration { get; set; }

        public virtual User User { get; set; }
    }
}
