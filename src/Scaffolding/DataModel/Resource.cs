﻿using System;
using System.Collections.Generic;

namespace Coscine.Database.DataModel
{
    public partial class Resource
    {
        public Resource()
        {
            MetadataExtractions = new HashSet<MetadataExtraction>();
            ProjectResources = new HashSet<ProjectResource>();
            ResourceDisciplines = new HashSet<ResourceDiscipline>();
            PublicationRequests = new HashSet<ProjectPublicationRequest>();
        }

        public Guid Id { get; set; }
        public Guid TypeId { get; set; }
        public string ResourceName { get; set; }
        public string DisplayName { get; set; }
        public Guid? VisibilityId { get; set; }
        public Guid? LicenseId { get; set; }
        public string Keywords { get; set; }
        public string UsageRights { get; set; }
        public Guid? ResourceTypeOptionId { get; set; }
        public string Description { get; set; }
        public string ApplicationProfile { get; set; }
        public string FixedValues { get; set; }
        public Guid? Creator { get; set; }
        public string Archived { get; set; }
        public bool Deleted { get; set; }
        public DateTime? DateCreated { get; set; }
        public bool? MetadataLocalCopy { get; set; }

        public virtual License License { get; set; }
        public virtual ResourceType Type { get; set; }
        public virtual Visibility Visibility { get; set; }
        public virtual ICollection<MetadataExtraction> MetadataExtractions { get; set; }
        public virtual ICollection<ProjectResource> ProjectResources { get; set; }
        public virtual ICollection<ResourceDiscipline> ResourceDisciplines { get; set; }

        public virtual ICollection<ProjectPublicationRequest> PublicationRequests { get; set; }
    }
}
