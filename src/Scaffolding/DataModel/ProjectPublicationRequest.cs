﻿using System;
using System.Collections.Generic;

namespace Coscine.Database.DataModel
{
    public partial class ProjectPublicationRequest
    {
        public ProjectPublicationRequest()
        {
            Resources = new HashSet<Resource>();
        }

        public Guid Id { get; set; }
        public Guid ProjectId { get; set; }
        public string PublicationServiceRorId { get; set; }
        public Guid CreatorId { get; set; }
        public DateTime DateCreated { get; set; }
        public string Message { get; set; }

        public virtual User Creator { get; set; }
        public virtual Project Project { get; set; }

        public virtual ICollection<Resource> Resources { get; set; }
    }
}
