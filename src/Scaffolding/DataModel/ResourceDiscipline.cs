﻿using System;
using System.Collections.Generic;

namespace Coscine.Database.DataModel
{
    public partial class ResourceDiscipline
    {
        public Guid RelationId { get; set; }
        public Guid DisciplineId { get; set; }
        public Guid ResourceId { get; set; }

        public virtual Discipline Discipline { get; set; }
        public virtual Resource Resource { get; set; }
    }
}
