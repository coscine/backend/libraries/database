﻿using System;
using System.Collections.Generic;

namespace Coscine.Database.DataModel
{
    public partial class RdsS3resourceType
    {
        public Guid Id { get; set; }
        public string BucketName { get; set; }
        public string AccessKey { get; set; }
        public string SecretKey { get; set; }
        public string AccessKeyRead { get; set; }
        public string SecretKeyRead { get; set; }
        public string AccessKeyWrite { get; set; }
        public string SecretKeyWrite { get; set; }
        public string Endpoint { get; set; }
    }
}
