﻿using System;
using System.Collections.Generic;

namespace Coscine.Database.DataModel
{
    public partial class ResourceType
    {
        public ResourceType()
        {
            ProjectQuotas = new HashSet<ProjectQuota>();
            Resources = new HashSet<Resource>();
        }

        public Guid Id { get; set; }
        public string DisplayName { get; set; }
        public bool? Enabled { get; set; }
        public string Type { get; set; }
        public string SpecificType { get; set; }

        public virtual ICollection<ProjectQuota> ProjectQuotas { get; set; }
        public virtual ICollection<Resource> Resources { get; set; }
    }
}
