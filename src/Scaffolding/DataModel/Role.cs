﻿using System;
using System.Collections.Generic;

namespace Coscine.Database.DataModel
{
    public partial class Role
    {
        public Role()
        {
            Invitations = new HashSet<Invitation>();
            ProjectRoles = new HashSet<ProjectRole>();
        }

        public Guid Id { get; set; }
        public string DisplayName { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Invitation> Invitations { get; set; }
        public virtual ICollection<ProjectRole> ProjectRoles { get; set; }
    }
}
