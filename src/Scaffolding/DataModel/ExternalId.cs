﻿using System;
using System.Collections.Generic;

namespace Coscine.Database.DataModel
{
    public partial class ExternalId
    {
        public Guid RelationId { get; set; }
        public Guid UserId { get; set; }
        public Guid ExternalAuthenticatorId { get; set; }
        public string ExternalId1 { get; set; }
        public string Organization { get; set; }

        public virtual ExternalAuthenticator ExternalAuthenticator { get; set; }
        public virtual User User { get; set; }
    }
}
