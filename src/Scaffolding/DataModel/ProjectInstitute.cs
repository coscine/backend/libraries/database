﻿using System;
using System.Collections.Generic;

namespace Coscine.Database.DataModel
{
    public partial class ProjectInstitute
    {
        public Guid RelationId { get; set; }
        public Guid ProjectId { get; set; }
        public string OrganizationUrl { get; set; }

        public virtual Project Project { get; set; }
    }
}
