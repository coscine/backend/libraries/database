﻿using System;
using System.Collections.Generic;

namespace Coscine.Database.DataModel
{
    public partial class Title
    {
        public Title()
        {
            Users = new HashSet<User>();
        }

        public Guid Id { get; set; }
        public string DisplayName { get; set; }

        public virtual ICollection<User> Users { get; set; }
    }
}
