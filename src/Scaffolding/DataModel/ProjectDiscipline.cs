﻿using System;
using System.Collections.Generic;

namespace Coscine.Database.DataModel
{
    public partial class ProjectDiscipline
    {
        public Guid RelationId { get; set; }
        public Guid DisciplineId { get; set; }
        public Guid ProjectId { get; set; }

        public virtual Discipline Discipline { get; set; }
        public virtual Project Project { get; set; }
    }
}
