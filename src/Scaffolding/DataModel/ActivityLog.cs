﻿using System;
using System.Collections.Generic;

namespace Coscine.Database.DataModel
{
    public partial class ActivityLog
    {
        public Guid Id { get; set; }
        public string ApiPath { get; set; }
        public Guid UserId { get; set; }
        public DateTime ActivityTimestamp { get; set; }
        public string HttpAction { get; set; }
        public string ActionName { get; set; }
        public string ControllerName { get; set; }

        public virtual User User { get; set; }
    }
}
