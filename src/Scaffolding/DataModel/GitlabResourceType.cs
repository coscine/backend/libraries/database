﻿using System;
using System.Collections.Generic;

namespace Coscine.Database.DataModel
{
    public partial class GitlabResourceType
    {
        public Guid Id { get; set; }
        public string Branch { get; set; }
        public int GitlabProjectId { get; set; }
        public string RepoUrl { get; set; }
        public string ProjectAccessToken { get; set; }
        public bool TosAccepted { get; set; }
    }
}
