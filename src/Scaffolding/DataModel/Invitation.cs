﻿using System;
using System.Collections.Generic;

namespace Coscine.Database.DataModel
{
    public partial class Invitation
    {
        public Guid Id { get; set; }
        public Guid Project { get; set; }
        public Guid Issuer { get; set; }
        public Guid Role { get; set; }
        public string InviteeEmail { get; set; }
        public DateTime Expiration { get; set; }
        public Guid Token { get; set; }

        public virtual User IssuerNavigation { get; set; }
        public virtual Project ProjectNavigation { get; set; }
        public virtual Role RoleNavigation { get; set; }
    }
}
