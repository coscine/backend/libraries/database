﻿using System;
using System.Collections.Generic;

namespace Coscine.Database.DataModel
{
    public partial class ActivatedFeature
    {
        public Guid Id { get; set; }
        public Guid ProjectId { get; set; }
        public Guid FeatureId { get; set; }

        public virtual Feature Feature { get; set; }
        public virtual Project Project { get; set; }
    }
}
