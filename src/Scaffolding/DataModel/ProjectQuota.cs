﻿using System;
using System.Collections.Generic;

namespace Coscine.Database.DataModel
{
    public partial class ProjectQuota
    {
        public Guid RelationId { get; set; }
        public Guid ProjectId { get; set; }
        public Guid ResourceTypeId { get; set; }
        public int Quota { get; set; }
        public int MaxQuota { get; set; }

        public virtual Project Project { get; set; }
        public virtual ResourceType ResourceType { get; set; }
    }
}
