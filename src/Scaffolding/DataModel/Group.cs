﻿using System;
using System.Collections.Generic;

namespace Coscine.Database.DataModel
{
    public partial class Group
    {
        public Group()
        {
            GroupMemberships = new HashSet<GroupMembership>();
        }

        public Guid Id { get; set; }
        public string DisplayName { get; set; }

        public virtual ICollection<GroupMembership> GroupMemberships { get; set; }
    }
}
