﻿using System;
using System.Collections.Generic;

namespace Coscine.Database.DataModel
{
    public partial class Discipline
    {
        public Discipline()
        {
            ProjectDisciplines = new HashSet<ProjectDiscipline>();
            ResourceDisciplines = new HashSet<ResourceDiscipline>();
            UserDisciplines = new HashSet<UserDiscipline>();
        }

        public Guid Id { get; set; }
        public string Url { get; set; }
        public string DisplayNameDe { get; set; }
        public string DisplayNameEn { get; set; }

        public virtual ICollection<ProjectDiscipline> ProjectDisciplines { get; set; }
        public virtual ICollection<ResourceDiscipline> ResourceDisciplines { get; set; }
        public virtual ICollection<UserDiscipline> UserDisciplines { get; set; }
    }
}
