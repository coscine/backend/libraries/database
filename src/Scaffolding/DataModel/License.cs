﻿using System;
using System.Collections.Generic;

namespace Coscine.Database.DataModel
{
    public partial class License
    {
        public License()
        {
            Resources = new HashSet<Resource>();
        }

        public Guid Id { get; set; }
        public string DisplayName { get; set; }
        public string Url { get; set; }

        public virtual ICollection<Resource> Resources { get; set; }
    }
}
