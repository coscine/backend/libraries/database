﻿using System;
using System.Collections.Generic;

namespace Coscine.Database.DataModel
{
    public partial class ProjectResource
    {
        public Guid RelationId { get; set; }
        public Guid ResourceId { get; set; }
        public Guid ProjectId { get; set; }

        public virtual Project Project { get; set; }
        public virtual Resource Resource { get; set; }
    }
}
