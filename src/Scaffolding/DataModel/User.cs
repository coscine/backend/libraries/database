﻿using System;
using System.Collections.Generic;

namespace Coscine.Database.DataModel
{
    public partial class User
    {
        public User()
        {
            ActivityLogs = new HashSet<ActivityLog>();
            ApiTokens = new HashSet<ApiToken>();
            ContactChanges = new HashSet<ContactChange>();
            ExternalIds = new HashSet<ExternalId>();
            GroupMemberships = new HashSet<GroupMembership>();
            Invitations = new HashSet<Invitation>();
            ProjectPublicationRequests = new HashSet<ProjectPublicationRequest>();
            ProjectRoles = new HashSet<ProjectRole>();
            Tosaccepteds = new HashSet<Tosaccepted>();
            UserDisciplines = new HashSet<UserDiscipline>();
        }

        public Guid Id { get; set; }
        public string EmailAddress { get; set; }
        public string DisplayName { get; set; }
        public string Givenname { get; set; }
        public string Surname { get; set; }
        public string Entitlement { get; set; }
        public string Organization { get; set; }
        public Guid? TitleId { get; set; }
        public Guid? LanguageId { get; set; }
        public DateTime? LatestActivity { get; set; }
        public DateTime? DeletedAt { get; set; }

        public virtual Language Language { get; set; }
        public virtual Title Title { get; set; }
        public virtual ICollection<ActivityLog> ActivityLogs { get; set; }
        public virtual ICollection<ApiToken> ApiTokens { get; set; }
        public virtual ICollection<ContactChange> ContactChanges { get; set; }
        public virtual ICollection<ExternalId> ExternalIds { get; set; }
        public virtual ICollection<GroupMembership> GroupMemberships { get; set; }
        public virtual ICollection<Invitation> Invitations { get; set; }
        public virtual ICollection<ProjectPublicationRequest> ProjectPublicationRequests { get; set; }
        public virtual ICollection<ProjectRole> ProjectRoles { get; set; }
        public virtual ICollection<Tosaccepted> Tosaccepteds { get; set; }
        public virtual ICollection<UserDiscipline> UserDisciplines { get; set; }
    }
}
