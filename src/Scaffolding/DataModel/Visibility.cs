﻿using System;
using System.Collections.Generic;

namespace Coscine.Database.DataModel
{
    public partial class Visibility
    {
        public Visibility()
        {
            Projects = new HashSet<Project>();
            Resources = new HashSet<Resource>();
        }

        public Guid Id { get; set; }
        public string DisplayName { get; set; }

        public virtual ICollection<Project> Projects { get; set; }
        public virtual ICollection<Resource> Resources { get; set; }
    }
}
