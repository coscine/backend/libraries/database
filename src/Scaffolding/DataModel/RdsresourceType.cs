﻿using System;
using System.Collections.Generic;

namespace Coscine.Database.DataModel
{
    public partial class RdsresourceType
    {
        public Guid Id { get; set; }
        public string BucketName { get; set; }
        public string AccessKey { get; set; }
        public string SecretKey { get; set; }
        public string Endpoint { get; set; }
    }
}
