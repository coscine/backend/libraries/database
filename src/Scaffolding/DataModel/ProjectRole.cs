﻿using System;
using System.Collections.Generic;

namespace Coscine.Database.DataModel
{
    public partial class ProjectRole
    {
        public Guid RelationId { get; set; }
        public Guid ProjectId { get; set; }
        public Guid UserId { get; set; }
        public Guid RoleId { get; set; }

        public virtual Project Project { get; set; }
        public virtual Role Role { get; set; }
        public virtual User User { get; set; }
    }
}
