﻿using System;
using System.Collections.Generic;

namespace Coscine.Database.DataModel
{
    public partial class UserDiscipline
    {
        public Guid RelationId { get; set; }
        public Guid DisciplineId { get; set; }
        public Guid UserId { get; set; }

        public virtual Discipline Discipline { get; set; }
        public virtual User User { get; set; }
    }
}
