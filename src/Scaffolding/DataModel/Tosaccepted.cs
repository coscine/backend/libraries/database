﻿using System;
using System.Collections.Generic;

namespace Coscine.Database.DataModel
{
    public partial class Tosaccepted
    {
        public Guid RelationId { get; set; }
        public Guid UserId { get; set; }
        public string Version { get; set; }

        public virtual User User { get; set; }
    }
}
