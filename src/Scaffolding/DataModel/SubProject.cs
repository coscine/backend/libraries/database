﻿using System;
using System.Collections.Generic;

namespace Coscine.Database.DataModel
{
    public partial class SubProject
    {
        public Guid RelationId { get; set; }
        public Guid ProjectId { get; set; }
        public Guid SubProjectId { get; set; }

        public virtual Project Project { get; set; }
        public virtual Project SubProjectNavigation { get; set; }
    }
}
