using Coscine.Database.Helpers;
using Coscine.Migrations;
using System;

namespace Database.Actions
{
    public class Program
    {
        private const string STR_DB_NAME = "--name";
        private const string STR_DB_SOURCE = "--source";
        private const string STR_DB_USER = "--user";
        private const string STR_DB_PW = "--pw";

        private const string STR_ACTION_CREATE = "--action-create";
        private const string STR_ACTION_DROP = "--action-drop";

        private static string Instructions =
            $"Possible actions: \n" +
            $"{STR_ACTION_CREATE} {STR_DB_NAME} <DATABASE NAME> {STR_DB_SOURCE} <DATABASE_SOURCE> {STR_DB_USER} <DATABASE_USER_ID> {STR_DB_PW} <DATABASE_PASSWORD> \n" +
            $"{STR_ACTION_DROP} {STR_DB_NAME} <DATABASE NAME> {STR_DB_SOURCE} <DATABASE_SOURCE> {STR_DB_USER} <DATABASE_USER_ID> {STR_DB_PW} <DATABASE_PASSWORD> \n";

        private static CoscineMigrations migrator = new CoscineMigrations();
        private static DatabaseMasterHelper helper = new DatabaseMasterHelper();

        public static void Main(string[] args)
        {
            if (args.Length != 9)
            {
                OutputRedMessage($"The input is not valid.");
                PrintHelp();
            }
            else
            {
                bool pointer;
                switch (args[0].ToLower())
                {
                    // --action-create
                    case STR_ACTION_CREATE:
                        pointer = AssembleSettings(args);
                        if (pointer)
                            CreateTemporaryDatabase();
                        else
                        {
                            OutputRedMessage($"Could not finish the requested operation {args[0]}. Exiting.");
                            return;
                        }
                        break;

                    // --action-drop
                    case STR_ACTION_DROP:
                        pointer = AssembleSettings(args);
                        if (pointer)
                            DropTemporaryDatabase();
                        else
                        {
                            OutputRedMessage($"Could not finish the requested operation {args[0]}. Exiting.");
                            return;
                        }
                        break;

                    case "--help":
                        PrintHelp();
                        break;
                    default:
                        OutputRedMessage($"Please start your input with the keyword \"{STR_ACTION_CREATE}\" or \"{STR_ACTION_DROP}\".");
                        PrintHelp();
                        break;
                }
            }
        }

        private static void PrintHelp()
        {
            Console.WriteLine("These are the possible actions:");
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine(Instructions);
            Console.ResetColor();
        }

        private static void CreateTemporaryDatabase()
        {
            if (helper.DatabaseExists(helper.ConnectionSettings.Database))
                OutputYellowMessage($"Database {helper.ConnectionSettings.Database} already exists.");
            try
            {
                helper.EnsureDatabase(helper.ConnectionSettings.Database);
                migrator.MigrateUp();
                OutputGreenMessage($"Database {helper.ConnectionSettings.Database} created/updated successfully!");
            }
            catch (Exception e)
            {
                Console.WriteLine($"Something went wrong trying to create database {helper.ConnectionSettings.Database}.");
                Console.WriteLine("Error Log: " + e);
            }
        }

        private static void DropTemporaryDatabase()
        {
            if (helper.DatabaseExists(helper.ConnectionSettings.Database))
            {
                try
                {
                    OutputGreenMessage($"Found database {helper.ConnectionSettings.Database}.");
                    helper.KillConnectionsToDatabase(helper.ConnectionSettings.Database);
                    helper.DropDatabase(helper.ConnectionSettings.Database);
                    OutputGreenMessage($"Database {helper.ConnectionSettings.Database} deleted successfully!");
                }
                catch (Exception e)
                {
                    OutputRedMessage($"Something went wrong trying to delete database {helper.ConnectionSettings.Database}.");
                    Console.WriteLine("Error Message: " + e);
                }
            }
            else OutputYellowMessage($"Database {helper.ConnectionSettings.Database} does not exist!");
        }

        private static void OutputRedMessage(string message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(message);
            Console.ResetColor();
        }

        private static void OutputYellowMessage(string message)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(message);
            Console.ResetColor();
        }

        private static void OutputGreenMessage(string message)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(message);
            Console.ResetColor();
        }

        private static bool AssembleSettings(string[] args)
        {
            ConfigurationConnectionSettings settings = new ConfigurationConnectionSettings();
            // start from index = 1, everything after --action-create...
            for (int ind = 1; ind < args.Length; ind += 2)
            {
                switch (args[ind].ToLower())
                {
                    case STR_DB_NAME:
                        settings.Database = args[ind + 1];
                        break;
                    case STR_DB_SOURCE:
                        settings.DataSource = args[ind + 1];
                        break;
                    case STR_DB_USER:
                        settings.UserId = args[ind + 1];
                        break;
                    case STR_DB_PW:
                        settings.Password = args[ind + 1];
                        break;
                    default:
                        OutputYellowMessage($"The input {args[ind]} is not valid.");
                        PrintHelp();
                        return false;
                }
            }
            migrator.ConnectionSettings = settings;
            helper.ConnectionSettings = settings;
            OutputGreenMessage("Configuration settings successfully extracted from input.");
            return true;
        }
    }
}
