﻿using System;

namespace Coscine.Database.ReturnObjects
{
    [Serializable]
    public class OrganizationObject : IReturnObject
    {
        public string Url { get; set; }
        public string DisplayName { get; set; }

        public OrganizationObject(string url, string displayName)
        {
            Url = url;
            DisplayName = displayName;
        }
    }
}
