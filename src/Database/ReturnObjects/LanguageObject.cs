﻿using System;

namespace Coscine.Database.ReturnObjects
{
    public class LanguageObject : IReturnObject
    {
        public Guid Id { get; set; }

        public string DisplayName { get; set; }

        public string Abbreviation { get; set; }

        public LanguageObject(Guid id, string displayName, string abbreviation)
        {
            Id = id;
            DisplayName = displayName;
            Abbreviation = abbreviation;
        }
    }
}
