﻿using System;

namespace Coscine.Database.ReturnObjects
{
    [Serializable]
    public class RDSResourceTypeObject : ResourceTypeOptionObject
    {
        public Guid Id { get; set; }
        public string BucketName { get; set; }
        public int? Size { get; set; }

        public RDSResourceTypeObject(Guid id, string bucketName, int? size)
        {
            Id = id;
            BucketName = bucketName;
            Size = size;
        }
    }
}
