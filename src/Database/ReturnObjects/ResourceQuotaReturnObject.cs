﻿using System;

namespace Coscine.Database.ReturnObjects;

/// <summary>
/// Contains information about the quota of a resource.
/// </summary>    
public class ResourceQuotaReturnObject
{
    /// <summary>
    /// Id of the resoure.
    /// </summary>
    public Guid Id { get; set; }
    /// <summary>
    /// Display name of the resource.
    /// </summary>
    public string Name { get; set; }
    /// <summary>
    /// How much space is used by all files in a resource [Bytes].
    /// </summary>
    public QuotaDimObject Used { get; set; }
    /// <summary>
    /// How much space is used by all files in a resource [%].
    /// </summary>
    public float UsedPercentage { get; set; }
    /// <summary>
    /// How much space is reserved and is available to be taken by the resource [GiB]. This number represents its capacity.
    /// </summary>
    public QuotaDimObject Reserved { get; set; }
}
