﻿using System;

namespace Coscine.Database.ReturnObjects
{
    [Serializable]
    public class OrganizationCountObject : IReturnObject
    {
        public string Url { get; set; }
        public int Count { get; set; }

        public OrganizationCountObject(string url, int count)
        {
            Url = url;
            Count = count;
        }
    }
}
