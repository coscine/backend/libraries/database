﻿using System;

namespace Coscine.Database.ReturnObjects
{
    public class TitleObject : IReturnObject
    {
        public Guid Id { get; set; }

        public string DisplayName { get; set; }


        public TitleObject(Guid id, string displayName)
        {
            Id = id;
            DisplayName = displayName;
        }
    }
}
