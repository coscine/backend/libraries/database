﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Coscine.Database.ReturnObjects;

[JsonConverter(typeof(StringEnumConverter))]
public enum QuotaUnit
{
    [Description("The byte is a unit of digital information in computing and telecommunications that most commonly consists of eight bits.")]
    [EnumMember(Value = "https://qudt.org/vocab/unit/BYTE")]
    BYTE,

    [Description("The kibibyte is a multiple of the unit byte for digital information equivalent to 1024 bytes.")]
    [EnumMember(Value = "https://qudt.org/vocab/unit/KibiBYTE")]
    KibiBYTE,

    [Description("The mebibyte is a multiple of the unit byte for digital information equivalent to 1024^2 or 2^20 bytes.")]
    [EnumMember(Value = "https://qudt.org/vocab/unit/MebiBYTE")]
    MebiBYTE,

    [Description("The gibibyte is a multiple of the unit byte for digital information storage. The prefix gibi means 1024^3.")]
    [EnumMember(Value = "https://qudt.org/vocab/unit/GibiBYTE")]
    GibiBYTE,

    [Description("The tebibyte is a multiple of the unit byte for digital information. The prefix tebi means 1024^4.")]
    [EnumMember(Value = "https://qudt.org/vocab/unit/TebiBYTE")]
    TebiBYTE,

    [Description("The tebibyte is a multiple of the unit byte for digital information. The prefix tebi means 1024^5.")]
    [EnumMember(Value = "https://qudt.org/vocab/unit/PebiBYTE")]
    PebiBYTE,
}