﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace Coscine.Database.ReturnObjects
{
    public class WaterbutlerObject
    {
        public bool IsFolder { get; set; } = false;
        public string Absolutepath { get; set; } = null;
        public string Name { get; set; } = null;
        public string LastModified { get; set; }
        public string Created { get; set; }
        public string Size { get; set; }
        // Shallow! Only one level deep. 
        // Should the folder contain additional folders, they will be empty/null.
        public List<WaterbutlerObject> Content { get; set; } = null;

        public WaterbutlerObject()
        {

        }

        public WaterbutlerObject(string path, JToken data)
        {
            Absolutepath = path;
            Content = new List<WaterbutlerObject>();
            IsFolder = true;

            if (data == null)
            {
                throw new ArgumentNullException("The data for the WaterbutlerFolder cannot be null.");
            }

            foreach (var obj in data)
            {
                if (obj["type"].ToObject<string>() == "files")
                {
                    Content.Add(new WaterbutlerObject
                    {
                        IsFolder = obj["attributes"]["kind"].ToObject<string>() == "folder",
                        Absolutepath = obj["attributes"]["path"].ToObject<string>(),
                        Name = obj["attributes"]["name"].ToObject<string>(),
                        Created = obj["attributes"]["modified_utc"]?.ToObject<string>(),
                        LastModified = obj["attributes"]["LastModified_utc"]?.ToObject<string>(),
                        Size = obj["attributes"]["size"]?.ToObject<string>(),
                    });
                }
            }
        }
    }
}
