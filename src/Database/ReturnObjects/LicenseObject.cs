﻿using System;

namespace Coscine.Database.ReturnObjects
{
    [Serializable]
    public class LicenseObject : IReturnObject
    {
        public Guid Id { get; set; }

        public string DisplayName { get; set; }

        public LicenseObject(Guid id, string displayName)
        {
            Id = id;
            DisplayName = displayName;
        }
    }
}