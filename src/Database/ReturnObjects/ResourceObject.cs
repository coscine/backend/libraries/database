﻿using Coscine.Configuration;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace Coscine.Database.ReturnObjects
{
    [Serializable]
    public class ResourceObject : IReturnObject
    {
        public Guid Id { get; set; }
        // TODO: Needs a better fix
        public string PID { get { var configuration = new ConsulConfiguration(); return configuration.GetStringAndWait("coscine/global/epic/prefix") + "/" + Id.ToString(); } }
        public string DisplayName { get; set; }
        public string ResourceName { get; set; }
        public string Description { get; set; }
        public string Keywords { get; set; }
        public string UsageRights { get; set; }
        public ResourceTypeObject Type { get; set; }
        public IEnumerable<DisciplineObject> Disciplines { get; set; }
        public VisibilityObject Visibility { get; set; }
        public LicenseObject License { get; set; }
        public JObject ResourceTypeOption { get; set; }
        public string ApplicationProfile { get; set; }
        public JToken FixedValues { get; set; }
        public DateTime? DateCreated { get; set; }
        public Guid? Creator { get; set; }
        public bool Archived { get; set; }
        public bool Deleted { get; set; }

        public ResourceObject(Guid id, string displayName, string resourceName, string description, string keywords, string usageRights, ResourceTypeObject type, IEnumerable<DisciplineObject> disciplines, VisibilityObject visibility, LicenseObject license, JObject resourceTypeOption, string applicationProfile, JToken fixedValues, DateTime? dateCreated, Guid? creator = null, bool archived = false, bool deleted = false)
        {
            Id = id;

            DisplayName = displayName;
            ResourceName = resourceName;
            Description = description;
            Keywords = keywords;
            UsageRights = usageRights;

            Type = type;
            Disciplines = disciplines;
            Visibility = visibility;
            License = license;

            ResourceTypeOption = resourceTypeOption;

            ApplicationProfile = applicationProfile;
            FixedValues = fixedValues;
            DateCreated = dateCreated;
            Creator = creator;
            Archived = archived;
            Deleted = deleted;
        }
    }
}
