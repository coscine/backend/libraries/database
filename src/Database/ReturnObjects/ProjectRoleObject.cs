﻿using System;

namespace Coscine.Database.ReturnObjects
{
    [Serializable]
    public class ProjectRoleObject : IReturnObject
    {
        public Guid ProjectId { get; set; }
        public UserObject User { get; set; }
        public RoleObject Role { get; set; }

        public ProjectRoleObject(Guid projectId, UserObject user, RoleObject role)
        {
            ProjectId = projectId;
            User = user;
            Role = role;
        }

    }
}
