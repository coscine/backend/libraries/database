﻿namespace Coscine.Database.ReturnObjects;

public class QuotaDimObject
{
    public float Value { get; set; }
    public QuotaUnit Unit { get; set; }
}

