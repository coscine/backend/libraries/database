﻿using System;

namespace Coscine.Database.ReturnObjects
{
    public class VisibilityObject : IReturnObject
    {
        public Guid Id { get; set; }

        public string DisplayName { get; set; }

        public VisibilityObject(Guid id, string displayName)
        {
            Id = id;
            DisplayName = displayName;
        }
    }
}
