﻿using System;

namespace Coscine.Database.ReturnObjects;

/// <summary>
/// Return object for a project invitation.
/// </summary>
public class InvitationReturnObject
{
    /// <summary>
    /// The invitation id.
    /// </summary>
    public Guid Id { get; set; }

    /// <summary>
    /// When the invite will expire.
    /// </summary>
    public DateTime Expiration { get; set; }

    /// <summary>
    /// Email of the invitee.
    /// </summary>
    public string UserMail { get; set; }

    /// <summary>
    /// Id of the issuer.
    /// </summary>
    public Guid Issuer { get; set; }

    /// <summary>
    /// Id of the project.
    /// </summary>
    public Guid ProjectId { get; set; }

    /// <summary>
    /// Id of the target Role.
    /// </summary>
    public Guid RoleId { get; set; }
}