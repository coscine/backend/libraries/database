﻿using System;

namespace Coscine.Database.ReturnObjects
{
    [Serializable]
    public class S3ResourceTypeObject : ResourceTypeOptionObject
    {
        public Guid Id { get; set; }
        public string BucketName { get; set; }
        public string AccessKey { get; set; }
        public string SecretKey { get; set; }
        public string ResourceUrl { get; set; }

        public S3ResourceTypeObject(Guid id, string bucketName, string accessKey, string secretKey, string resourceUrl)
        {
            Id = id;
            BucketName = bucketName;
            AccessKey = accessKey;
            SecretKey = secretKey;
            ResourceUrl = resourceUrl;
        }
    }
}
