﻿using Coscine.Configuration;
using System;
using System.Collections.Generic;

namespace Coscine.Database.ReturnObjects
{

    [Serializable]
    public class ProjectObject : IReturnObject
    {
        public Guid Id { get; set; }

        public string PID { get { var configuration = new ConsulConfiguration(); return configuration.GetStringAndWait("coscine/global/epic/prefix") + "/" + Id.ToString(); } }

        public string Description { get; set; }
        public string DisplayName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Keywords { get; set; }

        public string ProjectName { get; set; }
        public string PrincipleInvestigators { get; set; }
        public string GrantId { get; set; }
        public string Slug { get; set; }
        public DateTime? DateCreated { get; set; }

        public IEnumerable<DisciplineObject> Disciplines { get; set; }
        public IEnumerable<OrganizationObject> Organizations { get; set; }
        public VisibilityObject Visibility { get; set; }

        public Guid ParentId { get; set; }
        public Guid? Creator { get; set; }
        public bool Deleted { get; set; }

        public ProjectObject(Guid id, string description, string displayName, DateTime startDate, DateTime endDate, string keywords, string projectName, string principleInvestigators, string grantId, IEnumerable<DisciplineObject> discipline, IEnumerable<OrganizationObject> organization, VisibilityObject visibility, string slug, DateTime? dateCreated, Guid parentId = new Guid(), Guid? creator = null, bool deleted = false)
        {
            Id = id;
            Description = description;
            DisplayName = displayName;
            StartDate = startDate;
            EndDate = endDate;
            Keywords = keywords;
            ParentId = parentId;
            Creator = creator;
            ProjectName = projectName;
            PrincipleInvestigators = principleInvestigators;
            GrantId = grantId;
            Slug = slug;


            Disciplines = discipline;
            Organizations = organization;
            Visibility = visibility;
            DateCreated = dateCreated;
            Deleted = deleted;
        }
    }
}
