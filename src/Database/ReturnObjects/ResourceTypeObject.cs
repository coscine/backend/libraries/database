﻿using System;

namespace Coscine.Database.ReturnObjects
{
    [Serializable]
    public class ResourceTypeObject : IReturnObject
    {
        public Guid Id { get; set; }

        public string DisplayName { get; set; }

        public ResourceTypeObject(Guid id, string displayName)
        {
            Id = id;
            DisplayName = displayName;
        }

    }
}
