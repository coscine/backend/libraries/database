﻿using Coscine.Database.DataModel;
using System;
using System.Collections.Generic;

namespace Coscine.Database.ReturnObjects
{
    [Serializable]
    public class OrganizationResourceListObject : IReturnObject
    {
        public string Url { get; set; }
        public List<Resource> Resources { get; set; }

        public OrganizationResourceListObject(string url, List<Resource> resources)
        {
            Url = url;
            Resources = resources;
        }
    }
}
