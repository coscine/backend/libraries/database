﻿using System;

namespace Coscine.Database.ReturnObjects
{
    [Serializable]
    public class ApiTokenObject : IReturnObject
    {
        public Guid TokenId { get; set; }
        public string Name { get; set; }
        public DateTime Created { get; set; }
        public DateTime Expires { get; set; }
    }
}