﻿using System;

namespace Coscine.Database.ReturnObjects
{
    public class ContactChangeObject : IReturnObject
    {
        public Guid RelationId { get; set; }

        public Guid UserId { get; set; }

        public string NewEmail { get; set; }

        public DateTime? EditDate { get; set; }

        public Guid ConfirmationToken { get; set; }


        public ContactChangeObject(Guid relationId, Guid userId, string newEmail, DateTime? editDate, Guid confirmationToken)
        {
            RelationId = relationId;
            UserId = userId;
            NewEmail = newEmail;
            EditDate = editDate;
            ConfirmationToken = confirmationToken;
        }
    }
}
