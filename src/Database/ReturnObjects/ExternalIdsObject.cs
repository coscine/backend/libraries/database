﻿using System;

namespace Coscine.Database.ReturnObjects
{
    [Serializable]
    public class ExternalIdsObject : IReturnObject
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public Guid ExternalAuthenticatorId { get; set; }
        public string Organization { get; set; }
        public string ExternalId { get; set; }

        public ExternalIdsObject(Guid id, Guid userId, Guid externalAuthenticatorId, string organization, string externalId)
        {
            Id = id;
            UserId = userId;
            ExternalAuthenticatorId = externalAuthenticatorId;
            Organization = organization;
            ExternalId = externalId;
        }
    }
}
