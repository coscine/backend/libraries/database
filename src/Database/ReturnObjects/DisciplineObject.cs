﻿using System;

namespace Coscine.Database.ReturnObjects
{
    public class DisciplineObject : IReturnObject
    {
        public Guid Id { get; set; }

        public string Url { get; set; }
        public string DisplayNameDe { get; set; }

        public string DisplayNameEn { get; set; }

        public DisciplineObject(Guid id, string url, string displayNameDe, string displayNameEn)
        {
            Id = id;
            Url = url;
            DisplayNameDe = displayNameDe;
            DisplayNameEn = displayNameEn;
        }
    }
}
