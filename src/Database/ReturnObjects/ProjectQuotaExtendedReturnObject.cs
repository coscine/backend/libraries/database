﻿using System;
using System.Collections.Generic;

namespace Coscine.Database.ReturnObjects;

/// <summary>
/// Contains information about the quota of a project by resource type.
/// </summary>
public class ProjectQuotaExtendedReturnObject
{
    /// <summary>
    /// Id of the resoure type.
    /// </summary>
    public Guid Id { get; set; }

    /// <summary>
    /// Display name of the resource type.
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// How much space is reserved by resources in total [GiB]. Is equal to the sum of all resource quota reserved values.
    /// </summary>
    public QuotaDimObject TotalReserved { get; set; }

    /// <summary>
    /// How much space is currently allocated and is available to be taken by resources [GiB] (See Database, Table 'ProjectQuotas', Column 'Quota').
    /// </summary>
    public QuotaDimObject Allocated { get; set; }

    /// <summary>
    /// How much maximum space is possible to be taken by resources [GiB] (See Database, Table 'ProjectQuotas', Column 'MaxQuota').
    /// </summary>
    public QuotaDimObject Maximum { get; set; }

    /// <summary>
    /// Resources quota for all individual resources of a resource type in a selected project.
    /// </summary>
    public IEnumerable<ResourceQuotaReturnObject> ResourcesQuota { get; set; }
}