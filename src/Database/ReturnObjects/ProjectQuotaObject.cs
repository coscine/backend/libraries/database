﻿using System;

namespace Coscine.Database.ReturnObjects
{
    [Serializable]
    public class ProjectQuotaObject : IReturnObject
    {
        public Guid RelationId { get; set; }

        public Guid ProjectId { get; set; }

        public ResourceTypeObject ResourceType { get; set; }

        public int Quotas { get; set; }

        public ProjectQuotaObject(Guid relationId, Guid projectId, ResourceTypeObject resourceType, int quotas)
        {
            RelationId = relationId;
            ProjectId = projectId;
            ResourceType = resourceType;
            Quotas = quotas;
        }
    }
}
