﻿using Coscine.Database.DataModel;
using System;

namespace Coscine.Database.ReturnObjects
{
    public class ActivatedFeatureObject : IReturnObject
    {
        public Guid Id { get; set; }
        public Guid ProjectId { get; set; }
        public Guid FeatureId { get; set; }

        public ActivatedFeatureObject(Guid id, Guid projectId, Guid featureId)
        {
            Id = id;
            ProjectId = projectId;
            FeatureId = featureId;
        }

        public ActivatedFeatureObject(ActivatedFeature activatedFeature)
        {
            Id = activatedFeature.Id;
            ProjectId = activatedFeature.ProjectId;
            FeatureId = activatedFeature.FeatureId;
        }
    }
}
