﻿using System;

namespace Coscine.Database.ReturnObjects
{
    [Serializable]
    public class LinkedResourceTypeObject : ResourceTypeOptionObject
    {
        public Guid Id { get; set; }

        public LinkedResourceTypeObject(Guid id)
        {
            Id = id;
        }
    }
}
