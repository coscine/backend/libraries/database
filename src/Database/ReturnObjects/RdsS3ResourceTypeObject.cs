﻿using System;

namespace Coscine.Database.ReturnObjects
{
    [Serializable]
    public class RdsS3ResourceTypeObject : ResourceTypeOptionObject
    {
        public Guid Id { get; set; }
        public string BucketName { get; set; }
        public string ReadAccessKey { get; set; }
        public string ReadSecretKey { get; set; }
        public string WriteAccessKey { get; set; }
        public string WriteSecretKey { get; set; }
        public string Endpoint { get; set; }
        public int? Size { get; set; }
    }
}