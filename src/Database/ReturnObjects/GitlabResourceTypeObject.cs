﻿using System;

namespace Coscine.Database.ReturnObjects
{
    [Serializable]
    public class GitlabResourceTypeObject : ResourceTypeOptionObject
    {
        public Guid Id { get; set; }
        public string Branch { get; set; }
        public int ProjectId { get; set; }
        public Uri RepoUrl { get; set; }
        public string AccessToken { get; set; }
        public bool TosAccepted { get; set; } = false;

        public GitlabResourceTypeObject(Guid id, string branchName, int projectId, string repoUrl, string accessToken, bool tosaccepted)
        {
            Id = id;
            Branch = branchName;
            ProjectId = projectId;
            RepoUrl = new Uri(repoUrl);
            AccessToken = accessToken;
            TosAccepted = tosaccepted;
        }
    }
}
