﻿using System;

namespace Coscine.Database.ReturnObjects
{
    [Serializable]
    public class RoleObject : IReturnObject
    {
        public Guid Id { get; set; }

        public string DisplayName { get; set; }

        public RoleObject(Guid id, string displayName)
        {
            Id = id;
            DisplayName = displayName;
        }
    }
}