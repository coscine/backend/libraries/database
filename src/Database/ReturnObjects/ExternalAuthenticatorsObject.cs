﻿using System;

namespace Coscine.Database.ReturnObjects
{
    [Serializable]
    public class ExternalAuthenticatorsObject : IReturnObject
    {
        public Guid Id { get; set; }

        public string DisplayName { get; set; }

        public ExternalAuthenticatorsObject(Guid id, string displayName)
        {
            Id = id;
            DisplayName = displayName;
        }
    }
}