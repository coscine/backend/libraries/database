﻿using Coscine.Database.DataModel;
using System;

namespace Coscine.Database.ReturnObjects
{
    public class FeatureObject : IReturnObject
    {
        public Guid Id { get; set; }
        public string SharepointId { get; set; }
        public string En { get; set; }
        public string De { get; set; }
        public bool Activated { get; set; }

        public FeatureObject(Guid id, string sharepointId, string displaynameEn, string displaynameDe, bool activated = false)
        {
            Id = id;
            SharepointId = sharepointId;
            En = displaynameEn;
            De = displaynameDe;
            Activated = activated;
        }

        public FeatureObject(Feature feature, bool activated = false)
        {
            Id = feature.Id;
            SharepointId = feature.SharepointId;
            En = feature.DisplaynameEn;
            De = feature.DisplaynameDe;
            Activated = activated;
        }
    }
}
