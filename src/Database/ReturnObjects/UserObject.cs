﻿using System;
using System.Collections.Generic;

namespace Coscine.Database.ReturnObjects
{
    [Serializable]
    public class UserObject : IReturnObject
    {
        public Guid Id { get; set; }

        public string DisplayName { get; set; }

        public string Givenname { get; set; }

        public string Surname { get; set; }

        public string EmailAddress { get; set; }

        public bool HasProjectRole { get; set; }

        public TitleObject Title { get; set; }

        public LanguageObject Language { get; set; }

        public string Organization { get; set; }

        public IEnumerable<DisciplineObject> Disciplines { get; set; }

        public bool IsRegistered { get; set; }

        public IEnumerable<ExternalAuthenticatorsObject> ExternalAuthenticators { get; set; }

        public UserObject(Guid id, string displayName, string givenname, string surname, string emailAddress, bool hasProjectRole = false, TitleObject title = null, LanguageObject language = null, string organization = "", IEnumerable<DisciplineObject> disciplines = null, bool isRegistered = false, IEnumerable<ExternalAuthenticatorsObject> externalAuthenticators = null)
        {
            Id = id;
            DisplayName = displayName;
            Givenname = givenname;
            Surname = surname;
            EmailAddress = emailAddress;

            Title = title;
            Language = language;
            Organization = organization;
            Disciplines = disciplines;

            HasProjectRole = hasProjectRole;
            IsRegistered = isRegistered;

            ExternalAuthenticators = externalAuthenticators;
        }
    }
}
