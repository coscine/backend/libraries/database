﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;


namespace Coscine.Database.Settings
{
    [Obsolete("Use Dependency injection pattern instead.")]
    public static class DataConnection
    {
        [Obsolete("Use Dependency injection pattern instead.")]
        public static ILinqToDBSettings DefaultSettings { get; set; }
    }

    [Obsolete("Leftover from migration. Remove.")]
    public interface ILinqToDBSettings
    {

    }

    [Obsolete("Leftover from migration. Remove.")]
    public interface IDataProviderSettings { }

    [Obsolete("Leftover from migration. Remove.")]
    public interface IConnectionStringSettings
    {
        string ConnectionString { get; set; }
        bool IsGlobal { get; }
        string Name { get; set; }
        string ProviderName { get; set; }
    }
}

namespace Coscine.Database.Util
{
    public static class Linq2DbMigration
    {
        [Obsolete("Please use DbSet<T>.Add instead.")]
        public static int Insert<T>(this DbContext db, T t) where T : class
        {
            return (int)db.Add(t).State;
        }

        [Obsolete("Please use DbSet<T>.RemoveRange instead.")]
        public static int Delete<T>(this DbContext db, IEnumerable<T> t) where T : class
        {
            db.RemoveRange(t);
            return (int)EntityState.Deleted;
        }


        [Obsolete("Please use DbSet<T>.Remove instead.")]
        public static int Delete<T>(this DbContext db, T t) where T : class
        {
            return (int)db.Remove(t).State;
        }
    }
}
