﻿namespace Coscine.Database.Util
{
    /// <summary>
    /// User Roles in Coscine (Database mirror)
    /// </summary>
    /// <remarks>ALWAYS KEEP UP TO DATE WITH THE DATABASE TABLE!</remarks>
    public class UserRoles
    {
        public static string Owner { get; } = "owner";
        public static string Member { get; } = "member";
        public static string Guest { get; } = "guest";
    }
}
