﻿using System;

namespace Coscine.Database.Util
{
    public class DatabaseConnection
    {
        public static void ConnectToDatabase(Action<CoscineDB> action)
        {
            using (var coscineDB = new CoscineDB())
            {
                action.Invoke(coscineDB);
                coscineDB.SaveChanges();
            }
        }

        public static T ConnectToDatabase<T>(Func<CoscineDB, T> func)
        {
            T result;
            using (var coscineDB = new CoscineDB())
            {
                result = func.Invoke(coscineDB);
                coscineDB.SaveChanges();
            }
            return result;
        }

    }
}
