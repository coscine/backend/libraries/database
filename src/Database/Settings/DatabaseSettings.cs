﻿namespace Coscine.Database.Settings
{
    public class DatabaseSettings

    {
        public DatabaseSettings()
        {
        }

        public DatabaseSettings(string dataSource, string databaseName, string userId, string password)
        {
            DataSource = dataSource;
            DatabaseName = databaseName;
            UserId = userId;
            Password = password;
        }

        public string DataSource { get; set; }
        public string DatabaseName { get; set; }
        public string UserId { get; set; }
        public string Password { get; set; }


        public string ConnectionStrings()
        {
            return $"Data Source={DataSource};Integrated Security=False;User ID={UserId};Password={Password};Database={DatabaseName}";
        }
    }
}