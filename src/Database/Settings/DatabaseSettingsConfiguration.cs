﻿using Coscine.Configuration;

namespace Coscine.Database.Settings
{
    public class DatabaseSettingsConfiguration : DatabaseSettings
    {
        public DatabaseSettingsConfiguration(IConfiguration configuration)
        {
            Configuration = configuration;
            LoadValuesFromConfiguration();
        }

        public string DbDataSourceKey { get; set; } = "coscine/global/db_data_source";
        public string DbNameKey { get; set; } = "coscine/global/db_name";
        public string DbUserIdKey { get; set; } = "coscine/global/db_user_id";
        public string DbPasswordKey { get; set; } = "coscine/global/db_password";

        public IConfiguration Configuration { get; set; }

        private void LoadValuesFromConfiguration()
        {
            DataSource = Configuration.GetStringAndWait(DbDataSourceKey);
            DatabaseName = Configuration.GetStringAndWait(DbNameKey);
            UserId = Configuration.GetStringAndWait(DbUserIdKey);
            Password = Configuration.GetStringAndWait(DbPasswordKey);
        }
    }
}
