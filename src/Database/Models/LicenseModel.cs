﻿using Coscine.Database.DataModel;
using System;

namespace Coscine.Database.Models
{
    public class LicenseModel : DatabaseModel<License>
    {
        public override System.Linq.Expressions.Expression<Func<License, Guid>> GetIdFromObject()
        {
            return (license) => license.Id;
        }

        public override Microsoft.EntityFrameworkCore.DbSet<License> GetITableFromDatabase(CoscineDB db)
        {
            return db.Licenses;
        }

        public override void SetObjectId(License databaseObject, Guid id)
        {
            databaseObject.Id = id;
        }
    }
}
