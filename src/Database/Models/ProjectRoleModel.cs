﻿using Coscine.Database.DataModel;
using Coscine.Database.ReturnObjects;

using System;
using System.Linq;
using System.Linq.Expressions;

namespace Coscine.Database.Models
{
    public class ProjectRoleModel : DatabaseModel<ProjectRole>
    {
        public ProjectRole SetFromObject(ProjectRoleObject projectRoleObject)
        {
            // Remove existing roles if they exist
            var existingRoles = GetAllWhere((dbProjectRole) => dbProjectRole.ProjectId == projectRoleObject.ProjectId && dbProjectRole.UserId == projectRoleObject.User.Id);

            if (existingRoles.Count() > 0)
            {
                foreach (var role in existingRoles)
                {
                    CheckIfLastOwnerWillBeRemoved(role.RoleId, projectRoleObject.ProjectId);
                    Delete(role);
                }
            }
            ProjectRole projectRole = new ProjectRole()
            {
                ProjectId = projectRoleObject.ProjectId,
                UserId = projectRoleObject.User.Id,
                RoleId = projectRoleObject.Role.Id
            };
            Insert(projectRole);
            return projectRole;
        }

        public void CheckIfLastOwnerWillBeRemoved(Guid roleId, Guid projectId)
        {
            RoleModel roleModel = new RoleModel();
            var ownerRole = roleModel.GetOwnerRole();

            if (roleId == ownerRole.Id)
            {
                var moreThanOneOwnerExists = GetAllWhere((projectRole) =>
                    projectRole.ProjectId == projectId
                    && projectRole.RoleId == ownerRole.Id
                ).Count() > 1;
                if (!moreThanOneOwnerExists)
                {
                    throw new Exception("The last owner cannot be removed!");
                }
            }
        }

        public override Expression<Func<ProjectRole, Guid>> GetIdFromObject()
        {
            return databaseObject => databaseObject.RelationId;
        }

        public override Microsoft.EntityFrameworkCore.DbSet<ProjectRole> GetITableFromDatabase(CoscineDB db)
        {
            return db.ProjectRoles;
        }

        public override void SetObjectId(ProjectRole databaseObject, Guid id)
        {
            databaseObject.RelationId = id;
        }

        public Guid? GetGetUserRoleForProject(Guid projectId, Guid userId)
        {
            return GetAllWhere((projectRole) => projectRole.ProjectId == projectId && projectRole.UserId == userId)
                    .Select((projectRole) => projectRole.RoleId).First();
        }
    }
}
