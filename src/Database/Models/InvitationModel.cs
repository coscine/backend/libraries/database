﻿using Coscine.Database.DataModel;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq.Expressions;

namespace Coscine.Database.Models
{
    public class InvitationModel : DatabaseModel<Invitation>
    {
        public override Expression<Func<Invitation, Guid>> GetIdFromObject()
        {
            return databaseObject => databaseObject.Id;
        }

        public override DbSet<Invitation> GetITableFromDatabase(CoscineDB db)
        {
            return db.Invitations;
        }

        public override void SetObjectId(Invitation databaseObject, Guid id)
        {
            databaseObject.Id = id;
        }

        public Invitation GetByToken(Guid token)
        {
            return GetWhere(x => x.Token == token);
        }

        public Guid CreateInvitation(Guid project, Guid issuer, Guid roleId, string inviteeEmail)
        {
            var token = Guid.NewGuid();

            Insert(
                new Invitation
                {
                    Id = Guid.NewGuid(),
                    Project = project,
                    Issuer = issuer,
                    Role = roleId,
                    InviteeEmail = inviteeEmail,
                    Expiration = DateTime.UtcNow.AddDays(7),
                    Token = token,

                }
            );

            return token;
        }
    }
}
