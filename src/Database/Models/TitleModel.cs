﻿using Coscine.Database.DataModel;

using System;
using System.Linq.Expressions;

namespace Coscine.Database.Models
{
    public class TitleModel : DatabaseModel<Title>
    {
        public override Expression<Func<Title, Guid>> GetIdFromObject()
        {
            return (title) => title.Id;
        }

        public override Microsoft.EntityFrameworkCore.DbSet<Title> GetITableFromDatabase(CoscineDB db)
        {
            return db.Titles;
        }

        public override void SetObjectId(Title databaseObject, Guid id)
        {
            databaseObject.Id = id;
        }
    }
}
