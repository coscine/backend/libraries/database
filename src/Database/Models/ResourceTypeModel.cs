﻿using Coscine.Database.DataModel;

using System;
using System.Linq.Expressions;

namespace Coscine.Database.Models
{
    public class ResourceTypeModel : DatabaseModel<ResourceType>
    {
        public override Expression<Func<ResourceType, Guid>> GetIdFromObject()
        {
            return databaseObject => databaseObject.Id;
        }

        public override Microsoft.EntityFrameworkCore.DbSet<ResourceType> GetITableFromDatabase(CoscineDB db)
        {
            return db.ResourceTypes;
        }

        public override void SetObjectId(ResourceType databaseObject, Guid id)
        {
            databaseObject.Id = id;
        }
    }
}
