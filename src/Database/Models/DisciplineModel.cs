﻿using Coscine.Database.DataModel;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq.Expressions;

namespace Coscine.Database.Models
{
    public class DisciplineModel : DatabaseModel<Discipline>
    {
        public override Expression<Func<Discipline, Guid>> GetIdFromObject()
        {
            return (discipline) => discipline.Id;
        }

        public override DbSet<Discipline> GetITableFromDatabase(CoscineDB db)
        {
            return db.Disciplines;
        }

        public override void SetObjectId(Discipline databaseObject, Guid id)
        {
            databaseObject.Id = id;
        }
    }
}
