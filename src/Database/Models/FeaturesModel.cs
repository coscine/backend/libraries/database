﻿using Coscine.Database.DataModel;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq.Expressions;

namespace Coscine.Database.Models
{
    public class FeaturesModel : DatabaseModel<Feature>
    {
        public override Expression<Func<Feature, Guid>> GetIdFromObject()
        {
            return databaseObject => databaseObject.Id;
        }

        public override DbSet<Feature> GetITableFromDatabase(CoscineDB db)
        {
            return db.Features;
        }

        public override void SetObjectId(Feature databaseObject, Guid id)
        {
            databaseObject.Id = id;
        }
    }
}
