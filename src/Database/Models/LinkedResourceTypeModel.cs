﻿using Coscine.Database.DataModel;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Coscine.Database.Models
{
    public class LinkedResourceTypeModel : DatabaseModel<LinkedResourceType>
    {
        public override Expression<Func<LinkedResourceType, Guid>> GetIdFromObject()
        {
            return (linkedResourceType) => linkedResourceType.Id;
        }

        public override Microsoft.EntityFrameworkCore.DbSet<LinkedResourceType> GetITableFromDatabase(CoscineDB db)
        {
            return db.LinkedResourceTypes;
        }

        public override void SetObjectId(LinkedResourceType databaseObject, Guid id)
        {
            databaseObject.Id = id;
        }

        public Dictionary<string, string> GetResourceTypeOptions(Guid id)
        {
            return new Dictionary<string, string>();
        }
    }
}
