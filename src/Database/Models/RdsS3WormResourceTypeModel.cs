﻿using Coscine.Database.DataModel;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Coscine.Database.Models
{
    public class RdsS3WormResourceTypeModel : DatabaseModel<RdsS3wormResourceType>
    {
        public override Expression<Func<RdsS3wormResourceType, Guid>> GetIdFromObject()
        {
            return (rdsResourceType) => rdsResourceType.Id;
        }

        public override Microsoft.EntityFrameworkCore.DbSet<RdsS3wormResourceType> GetITableFromDatabase(CoscineDB db)
        {
            return db.RdsS3wormResourceTypes;
        }

        public override void SetObjectId(RdsS3wormResourceType databaseObject, Guid id)
        {
            databaseObject.Id = id;
        }

        public Dictionary<string, string> GetResourceTypeOptions(Guid id)
        {
            var resourceType = GetById(id);
            var dictionary = new Dictionary<string, string>
            {
                { "accessKey", resourceType.AccessKey },
                { "secretKey", resourceType.SecretKey },
                { "accessKeyRead", resourceType.AccessKeyRead },
                { "secretKeyRead", resourceType.SecretKeyRead },
                { "accessKeyWrite", resourceType.AccessKeyWrite },
                { "secretKeyWrite", resourceType.SecretKeyWrite },
                { "bucketname", resourceType.BucketName },
                { "endpoint", resourceType.Endpoint }
            };
            return dictionary;
        }
    }
}