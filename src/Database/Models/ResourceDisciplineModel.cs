﻿using Coscine.Database.DataModel;

using System;
using System.Linq.Expressions;

namespace Coscine.Database.Models
{
    public class ResourceDisciplineModel : DatabaseModel<ResourceDiscipline>
    {
        public override Expression<Func<ResourceDiscipline, Guid>> GetIdFromObject()
        {
            return (resourceDiscipline) => resourceDiscipline.RelationId;
        }

        public override Microsoft.EntityFrameworkCore.DbSet<ResourceDiscipline> GetITableFromDatabase(CoscineDB db)
        {
            return db.ResourceDisciplines;
        }

        public override void SetObjectId(ResourceDiscipline databaseObject, Guid id)
        {
            databaseObject.RelationId = id;
        }
    }
}
