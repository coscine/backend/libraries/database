﻿using Coscine.Database.DataModel;

using System;
using System.Linq.Expressions;

namespace Coscine.Database.Models
{
    public class ProjectDisciplineModel : DatabaseModel<ProjectDiscipline>
    {
        public override Expression<Func<ProjectDiscipline, Guid>> GetIdFromObject()
        {
            return (projectDiscipline) => projectDiscipline.RelationId;
        }

        public override Microsoft.EntityFrameworkCore.DbSet<ProjectDiscipline> GetITableFromDatabase(CoscineDB db)
        {
            return db.ProjectDisciplines;
        }

        public override void SetObjectId(ProjectDiscipline databaseObject, Guid id)
        {
            databaseObject.RelationId = id;
        }
    }
}
