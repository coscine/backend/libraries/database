﻿using Coscine.Database.DataModel;
using Coscine.Database.ReturnObjects;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Coscine.Database.Models
{
    public class ApiTokenModel : DatabaseModel<ApiToken>
    {
        public override Expression<Func<ApiToken, Guid>> GetIdFromObject()
        {
            return databaseObject => databaseObject.Id;
        }

        public override DbSet<ApiToken> GetITableFromDatabase(CoscineDB db)
        {
            return db.ApiTokens;
        }

        public override void SetObjectId(ApiToken databaseObject, Guid id)
        {
            databaseObject.Id = id;
        }

        public IEnumerable<ApiTokenObject> GetTokens(string userId)
        {
            return GetTokens(new Guid(userId));
        }

        public IEnumerable<ApiTokenObject> GetTokens(User user)
        {
            return GetTokens(user.Id);
        }

        public IEnumerable<ApiTokenObject> GetTokens(Guid userId)
        {
            return GetAllWhere((tableEntry) => tableEntry.UserId == userId)
                .Select(x => new ApiTokenObject { TokenId = x.Id, Created = x.IssuedAt, Expires = x.Expiration, Name = x.Name })
                .OrderBy(x => x.Created);
        }

        public ApiTokenObject GetToken(Guid userId, Guid tokenId)
        {
            var t = GetWhere(x => x.UserId == userId && x.Id == tokenId);
            if (t == null)
            {
                return null;
            }

            return new ApiTokenObject { TokenId = t.Id, Created = t.IssuedAt, Expires = t.Expiration, Name = t.Name };
        }

        public User GetUser(Guid tokenId)
        {
            return GetUser(GetById(tokenId));
        }

        public User GetUser(ApiToken token)
        {
            var userModel = new UserModel();
            return userModel.GetWhere(x => x.Id == token.UserId);
        }

        public void InsertToken(Guid tokenId, DateTime issuedAt, DateTime expires, Guid userId, string name)
        {
            Insert(new ApiToken
            {
                Id = tokenId,
                IssuedAt = issuedAt,
                Expiration = expires,
                UserId = userId,
                Name = name
            });
        }

        public int Revoke(Guid userId, Guid tokenId)
        {
            var apiToken = GetById(tokenId);
            if (apiToken.UserId == userId)
            {
                return Delete(apiToken);
            }
            else
            {
                return 0;
            }
        }
    }
}
