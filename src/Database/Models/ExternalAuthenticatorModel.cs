﻿using Coscine.Database.DataModel;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq.Expressions;

namespace Coscine.Database.Models
{
    public class ExternalAuthenticatorModel : DatabaseModel<ExternalAuthenticator>
    {
        public override Expression<Func<ExternalAuthenticator, Guid>> GetIdFromObject()
        {
            return (value) => value.Id;
        }

        public override DbSet<ExternalAuthenticator> GetITableFromDatabase(CoscineDB db)
        {
            return db.ExternalAuthenticators;
        }

        public override void SetObjectId(ExternalAuthenticator databaseObject, Guid id)
        {
            databaseObject.Id = id;
        }
    }
}
