﻿using Coscine.Database.DataModel;

using System;
using System.Linq.Expressions;

namespace Coscine.Database.Models
{
    public class ProjectInstituteModel : DatabaseModel<ProjectInstitute>
    {
        public override Expression<Func<ProjectInstitute, Guid>> GetIdFromObject()
        {
            return (projectInstitute) => projectInstitute.RelationId;
        }

        public override Microsoft.EntityFrameworkCore.DbSet<ProjectInstitute> GetITableFromDatabase(CoscineDB db)
        {
            return db.ProjectInstitutes;
        }

        public override void SetObjectId(ProjectInstitute databaseObject, Guid id)
        {
            databaseObject.RelationId = id;
        }
    }
}
