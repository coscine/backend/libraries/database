﻿using Coscine.Database.DataModel;
using System;

namespace Coscine.Database.Models
{
    public class LanguageModel : DatabaseModel<Language>
    {
        public override System.Linq.Expressions.Expression<Func<Language, Guid>> GetIdFromObject()
        {
            return (language) => language.Id;
        }

        public override Microsoft.EntityFrameworkCore.DbSet<Language> GetITableFromDatabase(CoscineDB db)
        {
            return db.Languages;
        }

        public override void SetObjectId(Language databaseObject, Guid id)
        {
            databaseObject.Id = id;
        }
    }
}
