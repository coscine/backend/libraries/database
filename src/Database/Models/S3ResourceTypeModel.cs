﻿using Coscine.Database.DataModel;

using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Coscine.Database.Models
{
    public class S3ResourceTypeModel : DatabaseModel<S3resourceType>
    {
        public override Expression<Func<S3resourceType, Guid>> GetIdFromObject()
        {
            return (rdsResourceType) => rdsResourceType.Id;
        }

        public override Microsoft.EntityFrameworkCore.DbSet<S3resourceType> GetITableFromDatabase(CoscineDB db)
        {
            return db.S3resourceTypes;
        }

        public override void SetObjectId(S3resourceType databaseObject, Guid id)
        {
            databaseObject.Id = id;
        }

        public Dictionary<string, string> GetResourceTypeOptions(Guid id)
        {
            var dictionary = new Dictionary<string, string>();
            var resourceType = GetById(id);
            dictionary.Add("accessKey", resourceType.AccessKey);
            dictionary.Add("secretKey", resourceType.SecretKey);
            dictionary.Add("bucketname", resourceType.BucketName);
            dictionary.Add("resourceUrl", resourceType.ResourceUrl);
            return dictionary;
        }
    }
}
