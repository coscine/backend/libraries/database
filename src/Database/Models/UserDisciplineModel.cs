﻿using Coscine.Database.DataModel;

using System;
using System.Linq.Expressions;

namespace Coscine.Database.Models
{
    public class UserDisciplineModel : DatabaseModel<UserDiscipline>
    {
        public override Expression<Func<UserDiscipline, Guid>> GetIdFromObject()
        {
            return (userDiscipline) => userDiscipline.RelationId;
        }

        public override Microsoft.EntityFrameworkCore.DbSet<UserDiscipline> GetITableFromDatabase(CoscineDB db)
        {
            return db.UserDisciplines;
        }

        public override void SetObjectId(UserDiscipline databaseObject, Guid id)
        {
            databaseObject.RelationId = id;
        }
    }
}
