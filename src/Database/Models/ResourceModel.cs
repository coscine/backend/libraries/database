using Coscine.Database.DataModel;
using Coscine.Database.ReturnObjects;
using Coscine.Database.Util;
using LinqKit;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Coscine.Database.Models
{
    public class ResourceModel : DatabaseModel<Resource>
    {
        public override Resource GetById(Guid id)
        {
            var expression = GetIdFromObject();
            return DatabaseConnection.ConnectToDatabase((db) =>
            {
                return
                    (from tableEntry in GetITableFromDatabase(db).AsExpandable()
                     where expression.Invoke(tableEntry) == id
                        && !tableEntry.Deleted
                     select tableEntry).FirstOrDefault();
            });
        }

        public Resource GetByIdIncludingDeleted(Guid id)
        {
            var expression = GetIdFromObject();
            return DatabaseConnection.ConnectToDatabase((db) =>
            {
                return
                    (from tableEntry in GetITableFromDatabase(db).AsExpandable()
                     where expression.Invoke(tableEntry) == id
                     select tableEntry).FirstOrDefault();
            });
        }
        public override Resource GetWhere(Expression<Func<Resource, bool>> whereClause)
        {
            return DatabaseConnection.ConnectToDatabase((db) =>
            {
                return
                    (from tableEntry in GetITableFromDatabase(db).AsExpandable()
                     where whereClause.Invoke(tableEntry)
                        && !tableEntry.Deleted
                     select tableEntry).FirstOrDefault();
            });
        }

        public override IEnumerable<Resource> GetAll()
        {
            return DatabaseConnection.ConnectToDatabase((db) =>
            {
                return
                    (from tableEntry in GetITableFromDatabase(db)
                     where !tableEntry.Deleted
                     select tableEntry).ToList();
            });
        }

        public override IEnumerable<Resource> GetAllWhere(Expression<Func<Resource, bool>> whereClause)
        {
            return DatabaseConnection.ConnectToDatabase((db) =>
            {
                return
                    (from tableEntry in GetITableFromDatabase(db).AsExpandable()
                     where whereClause.Invoke(tableEntry)
                        && !tableEntry.Deleted
                     select tableEntry).ToList();
            });
        }
        public override int Update(Resource databaseObject)
        {
            if (!databaseObject.Deleted)
            {
                return DatabaseConnection.ConnectToDatabase((db) =>
                {
                    return (int)db.Update(databaseObject).State;
                });
            }
            else
            {
                return 0;
            }
        }

        public Resource StoreFromObject(ResourceObject resourceObject, User user)
        {
            if (!resourceObject.Disciplines.Any() || resourceObject.ResourceTypeOption == null)
            {
                throw new ArgumentException("Discipline and ResourceTypeOption are necessary!");
            }

            Resource resource = new Resource()
            {
                DisplayName = resourceObject.DisplayName,
                ResourceName = resourceObject.ResourceName,
                Description = resourceObject.Description,
                Keywords = resourceObject.Keywords,
                UsageRights = resourceObject.UsageRights,
                TypeId = resourceObject.Type.Id,
                VisibilityId = resourceObject.Visibility.Id,
                ApplicationProfile = resourceObject.ApplicationProfile,
                FixedValues = resourceObject.FixedValues != null ? resourceObject.FixedValues.ToString() : "{}",
                // DateCreated is skipped here. Value set automatically by the database.
                Creator = user.Id
            };
            if (resourceObject.License != null)
            {
                resource.LicenseId = resourceObject.License.Id;
            }
            Insert(resource);

            resource.Type = new ResourceTypeModel().GetById(resourceObject.Type.Id);
            try
            {
                SetDisciplines(resource, resourceObject.Disciplines);
            }
            catch (Exception)
            {
                // Makes sure to delete all references, otherwise a delete is not possible
                HardDeleteResource(resource);
                throw;
            }

            return resource;
        }

        public int DeleteResource(Resource databaseObject)
        {
            databaseObject.Deleted = true;
            return DatabaseConnection.ConnectToDatabase((db) => (int)db.Update(databaseObject).State);
        }

        public bool IsDeleted(Guid id)
        {
            return DatabaseConnection.ConnectToDatabase((db) =>
            {
                return
                    (from tableEntry in GetITableFromDatabase(db)
                     where tableEntry.Id == id
                            && tableEntry.Deleted
                     select tableEntry).Count() == 1;
            });
        }

        public int HardDeleteResource(Resource resource)
        {
            ProjectResourceModel projectResourceModel = new ProjectResourceModel();
            foreach (var projectResource in projectResourceModel.GetAllWhere((projectResource) => projectResource.ResourceId == resource.Id))
            {
                projectResourceModel.Delete(projectResource);
            }

            ResourceDisciplineModel resourceDisciplineModel = new ResourceDisciplineModel();
            foreach (var resourceDiscipline in resourceDisciplineModel.GetAllWhere((resourceDicipline) => resourceDicipline.ResourceId == resource.Id))
            {
                resourceDisciplineModel.Delete(resourceDiscipline);
            }

            DeleteResourceTypeObject(resource);

            return Delete(resource);
        }

        public List<OrganizationCountObject> GetRDSBucketCountByOrganization()
        {
            var resourceListByOrganization = GetRDSResourceListByOrganization();
            var list = new List<OrganizationCountObject>();
            foreach (OrganizationResourceListObject rlo in resourceListByOrganization)
            {
                list.Add(new OrganizationCountObject(rlo.Url, rlo.Resources.Count));
            }
            return list;
        }

        public List<OrganizationResourceListObject> GetRDSResourceListByOrganization()
        {
            return DatabaseConnection.ConnectToDatabase((db) =>
            {
                return (from r in db.Resources
                        join pr in db.ProjectResources on r.Id equals pr.ResourceId into joinedPr
                        from jpr in joinedPr.DefaultIfEmpty()
                        join p in db.Projects on jpr.ProjectId equals p.Id into joinedP
                        from jp in joinedP.DefaultIfEmpty()
                        join pi in db.ProjectInstitutes on jp.Id equals pi.ProjectId into joinedPi
                        from jpi in joinedPi.DefaultIfEmpty()
                        join rt in db.ResourceTypes on r.TypeId equals rt.Id into joinedRt
                        from jrt in joinedRt.DefaultIfEmpty()

                        where !jp.Deleted && !r.Deleted &&
                        jrt.Type == "rds"
                        group r by jpi.OrganizationUrl into g
                        select new OrganizationResourceListObject(g.Key, g.ToList())).ToList();
            });
        }

        public Dictionary<string, string> GetResourceTypeOptions(Guid id)
        {
            var resource = GetById(id);
            var resourceType = new ResourceTypeModel().GetById(resource.TypeId);
            var resourceTypeOptionId = (Guid)resource.ResourceTypeOptionId;
            if (resourceType.Type == "gitlab")
            {
                return new GitlabResourceTypeModel().GetResourceTypeOptions(resourceTypeOptionId);
            }
            else if (resourceType.Type == "rds")
            {
                return new RDSResourceTypeModel().GetResourceTypeOptions(resourceTypeOptionId);
            }
            else if (resourceType.Type == "s3")
            {
                return new S3ResourceTypeModel().GetResourceTypeOptions(resourceTypeOptionId);
            }
            else if (resourceType.Type == "linked")
            {
                return new LinkedResourceTypeModel().GetResourceTypeOptions(resourceTypeOptionId);
            }
            else if (resourceType.Type == "rdss3")
            {
                return new RdsS3ResourceTypeModel().GetResourceTypeOptions(resourceTypeOptionId);
            }
            else if (resourceType.Type == "rdss3worm")
            {
                return new RdsS3WormResourceTypeModel().GetResourceTypeOptions(resourceTypeOptionId);
            }
            else
            {
                return new Dictionary<string, string>();
            }
        }

        private void SetDisciplines(Resource resource, IEnumerable<DisciplineObject> disciplines)
        {
            ResourceDisciplineModel resourceDisciplineModel = new ResourceDisciplineModel();
            foreach (var oldDiscipline in resourceDisciplineModel.GetAllWhere((resourceDiscipline) => resourceDiscipline.ResourceId == resource.Id))
            {
                resourceDisciplineModel.Delete(oldDiscipline);
            }
            foreach (var discipline in disciplines)
            {
                ResourceDiscipline resourceDiscipline = new ResourceDiscipline()
                {
                    DisciplineId = discipline.Id,
                    ResourceId = resource.Id
                };
                resourceDisciplineModel.Insert(resourceDiscipline);
            }
        }

        public bool HasAccess(User user, Resource resource, params string[] allowedAccess)
        {
            var projectId = new ProjectResourceModel().GetProjectForResource(resource.Id);
            IEnumerable<string> allowedAccessLabels = allowedAccess.Select(x => x.ToLower().Trim()).ToList();
            return DatabaseConnection.ConnectToDatabase((db) => (from relation in db.ProjectRoles
                                                                 where relation.ProjectId == projectId
                                                                     && relation.User.Id == user.Id
                                                                     && allowedAccessLabels.Contains(relation.Role.DisplayName.ToLower())
                                                                 select relation).Any());
        }

        public int UpdateByObject(Resource resource, ResourceObject resourceObject)
        {
            if (!resourceObject.Disciplines.Any() || resourceObject.ResourceTypeOption == null)
            {
                throw new ArgumentException("Discipline and ResourceTypeOption are necessary!");
            }

            if (resource.TypeId != resourceObject.Type.Id)
            {
                DeleteResourceTypeObject(resource);
            }

            resource.DisplayName = resourceObject.DisplayName;
            resource.ResourceName = resourceObject.ResourceName;
            resource.Description = resourceObject.Description;
            resource.Keywords = resourceObject.Keywords;
            resource.UsageRights = resourceObject.UsageRights;
            resource.TypeId = resourceObject.Type.Id;
            resource.Type = new ResourceTypeModel().GetById(resourceObject.Type.Id);
            resource.VisibilityId = resourceObject.Visibility.Id;
            if (resourceObject.License != null)
            {
                resource.LicenseId = resourceObject.License.Id;
            }
            else
            {
                resource.LicenseId = null;
            }
            resource.FixedValues = resourceObject.FixedValues != null ? resourceObject.FixedValues.ToString() : "{}";
            // Application Profile can not be altered after creation
            // Resource DateCreated can not be altered after creation
            // Creator can not be altered after creation

            SetDisciplines(resource, resourceObject.Disciplines);

            return Update(resource);
        }

        public void SetType(Resource resource)
        {
            if (resource.Type == null)
            {
                ResourceTypeModel resourceTypeModel = new ResourceTypeModel();
                resource.Type = resourceTypeModel.GetById(resource.TypeId);
            }
        }

        public void DeleteResourceTypeObject(Resource resource)
        {
            SetType(resource);
            if (resource.Type.Type == "rds" && resource.ResourceTypeOptionId != null)
            {
                RDSResourceTypeModel rdsResourceTypeModel = new RDSResourceTypeModel();
                rdsResourceTypeModel.Delete(rdsResourceTypeModel.GetById(resource.ResourceTypeOptionId.Value));
            }
            else if (resource.Type.Type == "s3" && resource.ResourceTypeOptionId != null)
            {
                S3ResourceTypeModel s3ResourceTypeModel = new S3ResourceTypeModel();
                s3ResourceTypeModel.Delete(s3ResourceTypeModel.GetById(resource.ResourceTypeOptionId.Value));
            }
            else if (resource.Type.Type == "gitlab" && resource.ResourceTypeOptionId != null)
            {
                GitlabResourceTypeModel gitlabResourceTypeModel = new GitlabResourceTypeModel();
                gitlabResourceTypeModel.Delete(gitlabResourceTypeModel.GetById(resource.ResourceTypeOptionId.Value));
            }
            else if (resource.Type.Type == "linked" && resource.ResourceTypeOptionId != null)
            {
                LinkedResourceTypeModel linkedResourceTypeModel = new LinkedResourceTypeModel();
                linkedResourceTypeModel.Delete(linkedResourceTypeModel.GetById(resource.ResourceTypeOptionId.Value));
            }
            else if (resource.Type.Type == "rdss3" && resource.ResourceTypeOptionId != null)
            {
                var rdsS3ResourceTypeModel = new RdsS3ResourceTypeModel();
                rdsS3ResourceTypeModel.Delete(rdsS3ResourceTypeModel.GetById(resource.ResourceTypeOptionId.Value));
            }
            else if (resource.Type.Type == "rdss3worm" && resource.ResourceTypeOptionId != null)
            {
                var rdsS3WormResourceTypeModel = new RdsS3WormResourceTypeModel();
                rdsS3WormResourceTypeModel.Delete(rdsS3WormResourceTypeModel.GetById(resource.ResourceTypeOptionId.Value));
            }
        }

        public ResourceObject CreateReturnObjectFromDatabaseObject(Resource resource)
        {
            SetType(resource);

            var disciplineModel = new DisciplineModel();
            var disciplines = disciplineModel.GetAllWhere((discipline) =>
                            (from relation in discipline.ResourceDisciplines//ResourceDisciplineDisciplineIdIds
                             where relation.ResourceId == resource.Id
                             select relation).Any())
                            .Select((discipline) => new DisciplineObject(discipline.Id, discipline.Url, discipline.DisplayNameDe, discipline.DisplayNameEn));

            if (resource.Visibility == null && resource.VisibilityId != null)
            {
                VisibilityModel visibilityModel = new VisibilityModel();
                resource.Visibility = visibilityModel.GetById(resource.VisibilityId.Value);
            }

            if (resource.License == null && resource.LicenseId != null)
            {
                LicenseModel licenseModel = new LicenseModel();
                resource.License = licenseModel.GetById(resource.LicenseId.Value);
            }

            ResourceTypeOptionObject resourceTypeOptionObject = null;
            if (resource.Type.Type == "rds" && resource.ResourceTypeOptionId != null)
            {
                RDSResourceTypeModel rdsResourceTypeModel = new RDSResourceTypeModel();
                var rdsResourceType = rdsResourceTypeModel.GetById(resource.ResourceTypeOptionId.Value);
                resourceTypeOptionObject = new RDSResourceTypeObject(rdsResourceType.Id, rdsResourceType.BucketName, null);
            }
            else if (resource.Type.Type == "s3" && resource.ResourceTypeOptionId != null)
            {
                S3ResourceTypeModel s3ResourceTypeModel = new S3ResourceTypeModel();
                var s3ResourceType = s3ResourceTypeModel.GetById(resource.ResourceTypeOptionId.Value);
                resourceTypeOptionObject = new S3ResourceTypeObject(s3ResourceType.Id, s3ResourceType.BucketName, null, null, s3ResourceType.ResourceUrl);
            }
            else if (resource.Type.Type == "gitlab" && resource.ResourceTypeOptionId != null)
            {
                GitlabResourceTypeModel gitlabResourceTypeModel = new GitlabResourceTypeModel();
                var gitlabResourceType = gitlabResourceTypeModel.GetById(resource.ResourceTypeOptionId.Value);
                resourceTypeOptionObject = new GitlabResourceTypeObject(gitlabResourceType.Id, gitlabResourceType.Branch, gitlabResourceType.GitlabProjectId, gitlabResourceType.RepoUrl, gitlabResourceType.ProjectAccessToken, gitlabResourceType.TosAccepted);
            }
            else if (resource.Type.Type == "linked" && resource.ResourceTypeOptionId != null)
            {
                LinkedResourceTypeModel linkedResourceTypeModel = new LinkedResourceTypeModel();
                var linkedResourceType = linkedResourceTypeModel.GetById(resource.ResourceTypeOptionId.Value);
                resourceTypeOptionObject = new LinkedResourceTypeObject(linkedResourceType.Id);
            }
            else if (resource.Type.Type == "rdss3" && resource.ResourceTypeOptionId != null)
            {
                var rdsS3ResourceTypeModel = new RdsS3ResourceTypeModel();
                var rdsS3ResourceType = rdsS3ResourceTypeModel.GetById(resource.ResourceTypeOptionId.Value);
                resourceTypeOptionObject = new RdsS3ResourceTypeObject
                {
                    Id = rdsS3ResourceType.Id,
                    BucketName = rdsS3ResourceType.BucketName,
                    Endpoint = rdsS3ResourceType.Endpoint,
                    ReadAccessKey = rdsS3ResourceType.AccessKeyRead,
                    ReadSecretKey = rdsS3ResourceType.SecretKeyRead,
                    WriteAccessKey = rdsS3ResourceType.AccessKeyWrite,
                    WriteSecretKey = rdsS3ResourceType.SecretKeyWrite,
                    Size = null
                };
            }
            else if (resource.Type.Type == "rdss3worm" && resource.ResourceTypeOptionId != null)
            {
                var rdsS3WormResourceTypeModel = new RdsS3WormResourceTypeModel();
                var rdsS3WormResourceType = rdsS3WormResourceTypeModel.GetById(resource.ResourceTypeOptionId.Value);
                resourceTypeOptionObject = new RdsS3ResourceTypeObject
                {
                    Id = rdsS3WormResourceType.Id,
                    BucketName = rdsS3WormResourceType.BucketName,
                    Endpoint = rdsS3WormResourceType.Endpoint,
                    ReadAccessKey = rdsS3WormResourceType.AccessKeyRead,
                    ReadSecretKey = rdsS3WormResourceType.SecretKeyRead,
                    WriteAccessKey = rdsS3WormResourceType.AccessKeyWrite,
                    WriteSecretKey = rdsS3WormResourceType.SecretKeyWrite,
                    Size = null
                };
            }

            return new ResourceObject(
                resource.Id,
                resource.DisplayName,
                resource.ResourceName,
                resource.Description,
                resource.Keywords,
                resource.UsageRights,
                new ResourceTypeObject(resource.Type.Id, resource.Type.SpecificType),
                disciplines,
                (resource.Visibility != null) ? new VisibilityObject(resource.Visibility.Id, resource.Visibility.DisplayName) : null,
                (resource.License != null) ? new LicenseObject(resource.License.Id, resource.License.DisplayName) : null,
                resourceTypeOptionObject == null ? new JObject() : JObject.FromObject(resourceTypeOptionObject),
                resource.ApplicationProfile,
                JToken.Parse(resource.FixedValues ?? "{}"),
                resource.DateCreated,
                resource.Creator,
                resource.Archived == "1",
                resource.Deleted
            );
        }

        public override Expression<Func<Resource, Guid>> GetIdFromObject()
        {
            return databaseObject => databaseObject.Id;
        }

        public override Microsoft.EntityFrameworkCore.DbSet<Resource> GetITableFromDatabase(CoscineDB db)
        {
            return db.Resources;
        }

        public override void SetObjectId(Resource databaseObject, Guid id)
        {
            databaseObject.Id = id;
        }

        public string GetMetadataCompleteness(ResourceObject resourceObject)
        {
            var maxCount = 0;
            var counted = 0;

            var projectProperties = typeof(ResourceObject).GetProperties();
            foreach (var property in projectProperties)
            {
                if (property == null
                    || property.PropertyType == typeof(Guid)
                    || property.Name == "Type"
                    || property.Name == "ResourceTypeOption"
                    || property.Name == "ApplicationProfile"
                    || property.Name == "FixedValues"
                    || property.Name == "Creator")
                {
                    continue;
                }

                maxCount++;

                if (property.PropertyType == typeof(string)
                    && property.GetValue(resourceObject) != null
                    && !string.IsNullOrEmpty(property.GetValue(resourceObject).ToString()))
                {
                    counted++;
                }
                else if (property.PropertyType == typeof(DateTime)
                    && property.GetValue(resourceObject) != null)
                {
                    counted++;
                }
                else if (property.PropertyType == typeof(ResourceTypeObject)
                    && property.GetValue(resourceObject) != null)
                {
                    counted++;
                }
                else if (property.PropertyType == typeof(IEnumerable<DisciplineObject>)
                    && property.GetValue(resourceObject) != null
                    && ((IEnumerable<DisciplineObject>)property.GetValue(resourceObject)).Any())
                {
                    counted++;
                }
                else if (property.PropertyType == typeof(VisibilityObject)
                    && property.GetValue(resourceObject) != null)
                {
                    counted++;
                }
                else if (property.PropertyType == typeof(LicenseObject)
                    && property.GetValue(resourceObject) != null)
                {
                    counted++;
                }
                else if (property.PropertyType == typeof(JObject)
                    && property.GetValue(resourceObject) != null)
                {
                    counted++;
                }
                else if (property.PropertyType == typeof(JToken)
                    && property.GetValue(resourceObject) != null)
                {
                    counted++;
                }
            }

            return $"{counted}/{maxCount}";
        }
    }
}