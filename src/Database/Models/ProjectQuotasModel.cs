﻿using Coscine.Database.DataModel;
using Coscine.Database.ReturnObjects;

using System;
using System.Linq.Expressions;

namespace Coscine.Database.Models
{
    public class ProjectQuotaModel : DatabaseModel<ProjectQuota>
    {
        public ProjectQuotaObject CreateReturnObjectFromDatabaseObject(ProjectQuota projectQuota)
        {
            if (projectQuota.ResourceType == null)
            {
                ResourceTypeModel resourceTypeModel = new ResourceTypeModel();
                projectQuota.ResourceType = resourceTypeModel.GetById(projectQuota.ResourceTypeId);
            }

            return new ProjectQuotaObject(
                projectQuota.RelationId,
                projectQuota.ProjectId,
                new ResourceTypeObject(
                    projectQuota.ResourceType.Id,
                    projectQuota.ResourceType.DisplayName
                ),
                projectQuota.Quota
            );
        }

        public override Expression<Func<ProjectQuota, Guid>> GetIdFromObject()
        {
            return (projectQuota) => projectQuota.RelationId;
        }

        public override Microsoft.EntityFrameworkCore.DbSet<ProjectQuota> GetITableFromDatabase(CoscineDB db)
        {
            return db.ProjectQuotas;
        }

        public override void SetObjectId(ProjectQuota databaseObject, Guid id)
        {
            databaseObject.RelationId = id;
        }
    }
}
