﻿using Coscine.Database.DataModel;
using Coscine.Database.ReturnObjects;
using System;
using System.Linq.Expressions;
using System.Web;

namespace Coscine.Database.Models
{
    [Obsolete("This should not be used")]
    public class MetadataModel : DatabaseModel<Resource>
    {

        public override Expression<Func<Resource, Guid>> GetIdFromObject()
        {
            throw new NotImplementedException();
        }

        public override void SetObjectId(Resource databaseObject, Guid id)
        {
            throw new NotImplementedException();
        }

        public override Microsoft.EntityFrameworkCore.DbSet<Resource> GetITableFromDatabase(CoscineDB db)
        {
            throw new NotImplementedException();
        }

        [Obsolete("This should not be used")]
        public Resource StoreFromObject(ResourceObject resourceObject)
        {
            Resource resource = new Resource()
            {
                DisplayName = resourceObject.DisplayName,
                ResourceName = resourceObject.ResourceName,
                Keywords = resourceObject.Keywords,
                UsageRights = resourceObject.UsageRights,
                TypeId = resourceObject.Type.Id,
                Type = new ResourceTypeModel().GetById(resourceObject.Type.Id),
                VisibilityId = resourceObject.Visibility.Id,
                LicenseId = resourceObject.License.Id
            };


            return resource;
        }

        [Obsolete("This should not be used")]
        public string GenerateId(string resourceId, string filename)
        {
            // Double UrlEncode since converting it to Uri executes one UrlDecode and Virtuoso
            // graph names don't support special characters
            var encodedFileName = HttpUtility.UrlEncode(HttpUtility.UrlEncode(filename));
            return $"https://purl.org/coscine/md/{resourceId}/{encodedFileName}/";
        }

        [Obsolete("This should not be used")]
        public Uri CreateUri(string graphName)
        {
            return new Uri(graphName);
        }
    }

}
