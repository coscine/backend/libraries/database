﻿using Coscine.Database.DataModel;
using Coscine.Database.Util;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace Coscine.Database.Models
{
    public class RoleModel : DatabaseModel<Role>
    {
        public override Expression<Func<Role, Guid>> GetIdFromObject()
        {
            return databaseObject => databaseObject.Id;
        }

        public override Microsoft.EntityFrameworkCore.DbSet<Role> GetITableFromDatabase(CoscineDB db)
        {
            return db.Roles;
        }

        public override void SetObjectId(Role databaseObject, Guid id)
        {
            databaseObject.Id = id;
        }

        public Role GetOwnerRole()
        {
            return DatabaseConnection.ConnectToDatabase((db) =>
                    (from tableEntry in GetITableFromDatabase(db)
                     where tableEntry.DisplayName.ToLower().Equals(UserRoles.Owner.ToLower())
                     select tableEntry).First());
        }
    }
}
