﻿using Coscine.Database.DataModel;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq.Expressions;

namespace Coscine.Database.Models
{
    public class ExternalIdModel : DatabaseModel<ExternalId>
    {
        public override Expression<Func<ExternalId, Guid>> GetIdFromObject()
        {
            return (value) => value.RelationId;
        }

        public override DbSet<ExternalId> GetITableFromDatabase(CoscineDB db)
        {
            return db.ExternalIds;
        }

        public override void SetObjectId(ExternalId databaseObject, Guid id)
        {
            databaseObject.RelationId = id;
        }
    }
}
