﻿using Coscine.Database.DataModel;
using Coscine.Database.ReturnObjects;
using Coscine.Database.Util;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Coscine.Database.Models
{
    public class UserModel : DatabaseModel<User>
    {
        public override Expression<Func<User, Guid>> GetIdFromObject()
        {
            return databaseObject => databaseObject.Id;
        }

        public override Microsoft.EntityFrameworkCore.DbSet<User> GetITableFromDatabase(CoscineDB db)
        {
            return db.Users;
        }

        public override void SetObjectId(User databaseObject, Guid Id)
        {
            databaseObject.Id = Id;
        }

        public UserObject CreateReturnObjectFromDatabaseObject(User user)
        {
            IEnumerable<DisciplineObject> disciplines = new List<DisciplineObject>();
            DisciplineModel disciplineModel = new DisciplineModel();
            disciplines = disciplineModel.GetAllWhere((discipline) => (from relation in discipline.UserDisciplines where relation.UserId == user.Id select relation).Any())
                                                .Select((discipline) => new DisciplineObject(discipline.Id, discipline.Url, discipline.DisplayNameDe, discipline.DisplayNameEn));

            var externalAuthenticatorModel = new ExternalAuthenticatorModel();
            var externalAuthenticators = externalAuthenticatorModel.GetAllWhere(
                (externalAuthenticator) =>
                    (from relation in externalAuthenticator.ExternalIds
                     where relation.UserId == user.Id
                     select relation).Any())
                        .Select((externalAuthenticator) =>
                            new ExternalAuthenticatorsObject(
                                externalAuthenticator.Id, externalAuthenticator.DisplayName));

            if (user.Title == null && user.TitleId.HasValue)
            {
                TitleModel titleModel = new TitleModel();
                user.Title = titleModel.GetById(user.TitleId.Value);
            }

            if (user.Language == null && user.LanguageId.HasValue)
            {
                LanguageModel languageModel = new LanguageModel();
                user.Language = languageModel.GetById(user.LanguageId.Value);
            }

            return new UserObject(
                user.Id,
                user.DisplayName,
                user.Givenname,
                user.Surname,
                user.EmailAddress,
                false,
                user.Title == null ? null : new TitleObject(user.Title.Id, user.Title.DisplayName),
                user.Language == null ? null : new LanguageObject(user.Language.Id, user.Language.DisplayName, user.Language.Abbreviation),
                user.Organization,
                disciplines,
                false,
                externalAuthenticators);
        }

        public int UpdateByObject(User user, UserObject userObject)
        {
            user.TitleId = userObject.Title?.Id;
            user.Givenname = userObject.Givenname;
            user.Surname = userObject.Surname;
            user.DisplayName = userObject.DisplayName;
            user.Organization = userObject.Organization;
            user.LanguageId = userObject.Language?.Id;
            SetDisciplines(user, userObject.Disciplines);
            return Update(user);
        }

        public List<Guid> GetActiveUserIds()
        {
            return DatabaseConnection.ConnectToDatabase((db) =>
            {
                return (from u in db.Users
                        join tos in db.Tosaccepteds on u.Id equals tos.UserId
                        select u.Id).Distinct().ToList();
            });
        }

        private void SetDisciplines(User user, IEnumerable<DisciplineObject> disciplines)
        {
            UserDisciplineModel userDisciplineModel = new UserDisciplineModel();
            foreach (var oldDiscipline in userDisciplineModel.GetAllWhere((userDiscipline) => userDiscipline.UserId == user.Id))
            {
                userDisciplineModel.Delete(oldDiscipline);
            }
            foreach (var discipline in disciplines)
            {
                userDisciplineModel.Insert(new UserDiscipline()
                {
                    UserId = user.Id,
                    DisciplineId = discipline.Id
                });
            }
        }
    }
}
