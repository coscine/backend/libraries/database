﻿using Coscine.Database.DataModel;

using System;
using System.Linq.Expressions;

namespace Coscine.Database.Models
{
    public class VisibilityModel : DatabaseModel<Visibility>
    {
        public override Expression<Func<Visibility, Guid>> GetIdFromObject()
        {
            return (visibility) => visibility.Id;
        }

        public override Microsoft.EntityFrameworkCore.DbSet<Visibility> GetITableFromDatabase(CoscineDB db)
        {
            return db.Visibilities;
        }

        public override void SetObjectId(Visibility databaseObject, Guid id)
        {
            databaseObject.Id = id;
        }
    }
}
