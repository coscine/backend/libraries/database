﻿using Coscine.Database.DataModel;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq.Expressions;

namespace Coscine.Database.Models
{
    public class MetadataExtractionModel : DatabaseModel<MetadataExtraction>
    {
        public override Expression<Func<MetadataExtraction, Guid>> GetIdFromObject()
        {
            return (metadataExtractionEntry) => metadataExtractionEntry.Id;
        }

        public override DbSet<MetadataExtraction> GetITableFromDatabase(CoscineDB db)
        {
            return db.MetadataExtractions;
        }

        public override void SetObjectId(MetadataExtraction databaseObject, Guid id)
        {
            databaseObject.Id = id;
        }
    }
}
