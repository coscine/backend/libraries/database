﻿using Coscine.Database.DataModel;
using System;
using System.Linq.Expressions;

namespace Coscine.Database.Models
{
    public class TOSModel : DatabaseModel<Tosaccepted>
    {
        public override Expression<Func<Tosaccepted, Guid>> GetIdFromObject()
        {
            return (tosAccepted) => tosAccepted.RelationId;
        }

        public override Microsoft.EntityFrameworkCore.DbSet<Tosaccepted> GetITableFromDatabase(CoscineDB db)
        {
            return db.Tosaccepteds;
        }

        public override void SetObjectId(Tosaccepted databaseObject, Guid id)
        {
            databaseObject.RelationId = id;
        }
    }
}
