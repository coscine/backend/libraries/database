﻿using Coscine.Database.DataModel;

using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Coscine.Database.Models
{
    public class RDSResourceTypeModel : DatabaseModel<RdsresourceType>
    {
        public override Expression<Func<RdsresourceType, Guid>> GetIdFromObject()
        {
            return (rdsResourceType) => rdsResourceType.Id;
        }

        public override Microsoft.EntityFrameworkCore.DbSet<RdsresourceType> GetITableFromDatabase(CoscineDB db)
        {
            return db.RdsresourceTypes;
        }

        public override void SetObjectId(RdsresourceType databaseObject, Guid id)
        {
            databaseObject.Id = id;
        }

        public Dictionary<string, string> GetResourceTypeOptions(Guid id)
        {
            var resourceType = GetById(id);
            var dictionary = new Dictionary<string, string>
            {
                { "accessKey", resourceType.AccessKey },
                { "secretKey", resourceType.SecretKey },
                { "bucketname", resourceType.BucketName },
                { "endpoint", resourceType.Endpoint }
            };
            return dictionary;
        }
    }
}