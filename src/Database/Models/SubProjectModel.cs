﻿using Coscine.Database.DataModel;

using System;
using System.Linq.Expressions;

namespace Coscine.Database.Models
{
    public class SubProjectModel : DatabaseModel<SubProject>
    {
        public override Expression<Func<SubProject, Guid>> GetIdFromObject()
        {
            return databaseObject => databaseObject.RelationId;
        }

        public override Microsoft.EntityFrameworkCore.DbSet<SubProject> GetITableFromDatabase(CoscineDB db)
        {
            return db.SubProjects;
        }

        public void LinkSubProject(Guid parentId, Guid childId)
        {
            Insert(new SubProject()
            {
                ProjectId = parentId,
                SubProjectId = childId,
            });
        }

        public override void SetObjectId(SubProject databaseObject, Guid id)
        {
            databaseObject.RelationId = id;
        }
    }
}
