﻿using Coscine.Database.DataModel;
using System;
using System.Linq.Expressions;


namespace Coscine.Database.Models
{
    public class LogModel : DatabaseModel<Log>
    {
        public override Expression<Func<Log, Guid>> GetIdFromObject()
        {
            return log => log.Id;
        }

        public override Microsoft.EntityFrameworkCore.DbSet<Log> GetITableFromDatabase(CoscineDB db)
        {
            return db.Logs;
        }

        public override void SetObjectId(Log databaseObject, Guid id)
        {
            databaseObject.Id = id;
        }
    }
}
