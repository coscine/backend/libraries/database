﻿using Coscine.Database.DataModel;

using System;
using System.Linq;
using System.Linq.Expressions;

namespace Coscine.Database.Models
{
    public class ProjectResourceModel : DatabaseModel<ProjectResource>
    {
        public override Expression<Func<ProjectResource, Guid>> GetIdFromObject()
        {
            return (projectResource) => projectResource.RelationId;
        }

        public override Microsoft.EntityFrameworkCore.DbSet<ProjectResource> GetITableFromDatabase(CoscineDB db)
        {
            return db.ProjectResources;
        }

        public override void SetObjectId(ProjectResource databaseObject, Guid id)
        {
            databaseObject.RelationId = id;
        }

        public Guid? GetProjectForResource(Guid resourceId)
        {
            return GetAllWhere((projectResource) => projectResource.ResourceId == resourceId)
                    .Select((projectResource) => projectResource.ProjectId).First();
        }
    }
}