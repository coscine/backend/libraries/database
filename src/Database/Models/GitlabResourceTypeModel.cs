﻿using Coscine.Database.DataModel;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Coscine.Database.Models
{
    public class GitlabResourceTypeModel : DatabaseModel<GitlabResourceType>
    {
        public override Expression<Func<GitlabResourceType, Guid>> GetIdFromObject()
        {
            return (gitlabResourceType) => gitlabResourceType.Id;
        }

        public override DbSet<GitlabResourceType> GetITableFromDatabase(CoscineDB db)
        {
            return db.GitlabResourceTypes;
        }

        public override void SetObjectId(GitlabResourceType databaseObject, Guid id)
        {
            databaseObject.Id = id;
        }

        public Dictionary<string, string> GetResourceTypeOptions(Guid id)
        {
            var resourceType = GetById(id);
            var dictionary = new Dictionary<string, string>
            {
                { "branchName", resourceType.Branch },
                { "projectId", resourceType.GitlabProjectId.ToString() },
                { "repositoryUrl", resourceType.RepoUrl },
                { "accessToken", resourceType.ProjectAccessToken }
            };
            return dictionary;
        }
    }
}
