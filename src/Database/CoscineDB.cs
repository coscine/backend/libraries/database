﻿using Coscine.Configuration;
using Coscine.Database.DataModel;
using Coscine.Database.Settings;
using Microsoft.EntityFrameworkCore;

namespace Coscine.Database
{
    public partial class CoscineDB : Model
    {
        public static DatabaseSettingsConfiguration DatabaseSettingsConfiguration { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (DatabaseSettingsConfiguration == null)
            {
                IConfiguration _configuration = new ConsulConfiguration();
                DatabaseSettingsConfiguration = new DatabaseSettingsConfiguration(_configuration);
            }

            optionsBuilder.UseSqlServer(DatabaseSettingsConfiguration.ConnectionStrings());
        }
    }
}
