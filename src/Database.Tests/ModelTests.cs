﻿using NUnit.Framework;
using System;

namespace Coscine.Database.Tests
{
    [TestFixture]
    public class ModelTests
    {
        private static string _databaseName;
        //    private IConfiguration _configuration;
        //    private static Guid _userId;

        //    [OneTimeSetUp]
        //    public void OneTimeSetUp()
        //    {
        //        // Collect the DatabaseName from the Script $env:DatabaseName=
        //        _databaseName = Environment.GetEnvironmentVariable("DatabaseName");
        //        _configuration = new ConsulConfiguration();
        //        DatabaseSettingsConfiguration settingsConfiguration = new DatabaseSettingsConfiguration(_configuration);
        //        settingsConfiguration.DatabaseName = _databaseName;
        //        CoscineDB.DatabaseSettingsConfiguration = settingsConfiguration;
        //    }

        //    [Test]
        //    public void TestRead()
        //    {
        //        // HOW TO EXECUTE SECOND?
        //        var abc = new UserModel().GetAll();
        //    }

        //    [Test]
        //    public void TestAdd()
        //    {
        //        // HOW TO EXECUTE FIRST?
        //        User testUser = new User()
        //        {
        //            Id = Guid.NewGuid(),
        //            DisplayName = "EF_TEST_DISPLAY_NAME",
        //            Givenname = "EF_TEST_GIVEN_NAME",
        //            Surname = "EF_TEST_SURNAME"
        //        };
        //        UserModel testUserModel = new UserModel();
        //        testUserModel.Insert(testUser);
        //        _userId = testUser.Id;
        //    }

        //    [Test]
        //    public void TestRemove()
        //    {
        //        // EXECUTE LAST; Delete created Test User in TestAdd().
        //    }
        //}
        [Test]
        public void TestCollectFromEnvironment()
        {
            _databaseName = Environment.GetEnvironmentVariable("DatabaseName");
            Assert.True(_databaseName.Length > 0);
        }
    }
}
