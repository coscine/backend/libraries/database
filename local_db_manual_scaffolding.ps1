$DB_DATA_SOURCE = consul kv get "coscine/global/db_data_source"
$DB_USER_ID = consul kv get "coscine/global/db_user_id"
$DB_PASSWORD = consul kv get "coscine/global/db_password"
$DB_NAME = consul kv get "coscine/global/db_name"

dotnet new tool-manifest --force
dotnet tool install dotnet-ef
dotnet tool update dotnet-ef

dotnet ef dbcontext scaffold "Data Source=$DB_DATA_SOURCE;Integrated Security=False;User ID=$DB_USER_ID;Password=$DB_PASSWORD;Database=$DB_NAME" Microsoft.EntityFrameworkCore.SqlServer -o DataModel -c "Model" -f --no-onconfiguring --project "src\Scaffolding"

# Remove the hardcoded Connection String inside \DataModel\Model.cs
Set-Content -Path ".\src\Scaffolding\DataModel\Model.txt" -Value (Get-Content -Path ".\src\Scaffolding\DataModel\Model.cs" | where { $_ | Select-String -Pattern '#warning' -NotMatch } | where { $_ | Select-String -Pattern 'Data Source=' -NotMatch } ) -Force
Set-Content -Path ".\src\Scaffolding\DataModel\Model.cs" -Value (Get-Content -Path ".\src\Scaffolding\DataModel\Model.txt") -Force
Remove-Item ".\src\Scaffolding\DataModel\Model.txt"
